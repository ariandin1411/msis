<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('product_no')->nullable();
            $table->string('product_name')->nullable();
            $table->integer('company_id')->default(0);
            $table->decimal('buy_price', 16, 2)->default(0);
            $table->decimal('sale_price', 16, 2)->default(0);
            $table->decimal('tax', 16, 2)->default(0);
            $table->integer('insert_by');
            $table->integer('update_by')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
