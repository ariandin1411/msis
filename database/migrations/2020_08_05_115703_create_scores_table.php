<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('subject_id');
            $table->integer('teacher_id');
            $table->integer('class_id');
            $table->decimal('score_total', 16,2)->default(0);
            $table->tinyInteger('active')->default(1);
            $table->integer('company_id')->nullable();
            $table->integer('year_id')->nullable();
            $table->tinyInteger('semester')->nullable();
            $table->integer('insert_by')->default(1);
            $table->integer('update_by')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scores');
    }
}
