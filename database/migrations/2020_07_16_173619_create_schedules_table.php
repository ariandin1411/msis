<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->id();
            $table->integer('subject_id');
            $table->integer('user_id');
            $table->integer('lesson_hour_id');
            $table->string('day_name');
            $table->string('schedule_type')->nullable();
            $table->integer('year_id');
            $table->integer('semester')->nullable();
            $table->integer('class_id');
            $table->integer('company_id')->default(0);
            $table->tinyInteger('active')->default(1);
            $table->integer('insert_by');
            $table->integer('update_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
