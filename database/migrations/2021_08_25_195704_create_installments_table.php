<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstallmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('installments', function (Blueprint $table) {
            $table->id();
            $table->integer('invoice_id');
            $table->integer('user_id');
            $table->integer('prod_id');
            $table->decimal('installment_amount', 16, 2)->nullable();
            $table->decimal('loan_amount', 16, 2)->nullable();
            $table->decimal('rest_of_loan', 16, 2)->nullable();
            $table->integer('active')->default(1);
            $table->integer('insert_by');
            $table->integer('update_by')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('installments');
    }
}
