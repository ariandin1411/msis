<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserGuardiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_guardians', function (Blueprint $table) {
            $table->id();
            $table->integer('student_user_id');
            $table->integer('guardian_user_id');
            $table->integer('company_id')->nullable();
            $table->integer('active')->default(1);
            $table->integer('insert_by');
            $table->integer('update_by')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_guardians');
    }
}
