<?php

use Illuminate\Database\Seeder;

class PayWaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //BMRI
        DB::table('pay_ways')->insert([
            'bank_code' => 'BMRI',
            'description' => '<h3>ATM Mandiri</h3>
            <ul>
            <li>Input kartu ATM dan PIN Anda</li>
            <li>Pilih Menu Bayar/Beli</li>
            <li>Pilih Lainnya</li>
            <li>Pilih Multi Payment</li>
            <li>Input 70014 sebagai Kode Institusi</li>
            <li>Input Virtual Account Number, misal. 70014XXXXXXXXXXX</li>
            <li>Pilih Benar</li>
            <li>Pilih Ya</li>
            <li>Pilih Ya</li>
            <li>Ambil bukti bayar Anda</li>
            <li>Selesai</li>
            </ul>
            <h3>Mobile Banking Mandiri</h3>
            <ul>
            <li>Login Mobile Banking</li>
            <li>Pilih Bayar</li>
            <li>Pilih Multi Payment</li>
            <li>Input Transferpay sebagai Penyedia Jasa</li>
            <li>Input Nomor Virtual Account, misal. 70014XXXXXXXXXXX</li>
            <li>Pilih Lanjut</li>
            <li>Input OTP and PIN</li>
            <li>Pilih OK</li>
            <li>Bukti bayar ditampilkan</li>
            <li>Selesai</li>
            </ul>
            <h3>Internet Banking Mandiri</h3>
            <ul>
            <li>Login Internet Banking</li>
            <li>Pilih Bayar</li>
            <li>Pilih Multi Payment</li>
            <li>Input Transferpay sebagai Penyedia Jasa</li>
            <li>Input Nomor Virtual Account, misal. 70014XXXXXXXXXXX sebagai Kode Bayar</li>
            <li>Ceklis IDR</li>
            <li>Klik Lanjutkan</li>
            <li>Bukti bayar ditampilkan</li>
            <li>Selesai</li>
            </ul>',
            'created_at' => date('Y-m-d H:i:s')
        ]);

    
        //BNIN
        DB::table('pay_ways')->insert([
            'bank_code' => 'BNIN',
            'description' => '<h3>ATM BNI</h3>
            <ul>
            <li>Masukkan Kartu Anda.</li>
            <li>Pilih Bahasa</li>
            <li>Masukkan PIN ATM Anda.</li>
            <li>Pilih Menu Lainnya.</li>
            <li>Pilih Transfer.</li>
            <li>Pilih Jenis rekening yang akan Anda gunakan (Contoh; Dari Rekening Tabungan).</li>
            <li>Pilih Virtual Account Billing</li>
            <li>Masukkan Nomor Virtual Account Anda (contoh: 123456789012XXXX).</li>
            <li>Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi.</li>
            <li>Konfirmasi, apabila telah sesuai, lanjutkan transaksi.</li>
            <li>Transaksi Anda telah selesai.</li>
            </ul>
            <h3>Mobile Banking BNI</h3>
            <ul>
            <li>Akses BNI Mobile Banking dari handphone kemudian masukkan user ID dan password.</li>
            <li>Pilih menu Transfer.</li>
            <li>Pilih menu Virtual Account Billing kemudian pilih rekening debet.</li>
            <li>Masukkan Nomor Virtual Account Anda (contoh: 123456789012XXXX) pada menu input baru.</li>
            <li>Input Nomor Virtual Account, misal. 70014XXXXXXXXXXX</li>
            <li>Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi.</li>
            <li>Konfirmasi transaksi dan masukkan Password Transaksi.</li>
            <li>Pembayaran Anda Telah Berhasil.</li>
            </ul>
            <h3>iBank Personal BNI</h3>
            <ul>
            <li>Ketik alamat https://ibank.bni.co.id kemudian klik Enter.</li>
            <li>Masukkan User ID dan Password.</li>
            <li>Pilih menu Transfer.</li>
            <li>Pilih Virtual Account Billing.</li>
            <li>Kemudian masukan Nomor Virtual Account Anda (contoh: 123456789012XXXX) yang hendak dibayarkan. Lalu pilih rekening debet yang akan digunakan. Kemudian tekan lanjut</li>
            <li>Kemudin tagihan yang harus dibayarkan akan muncul pada layar konfirmasi.</li>
            <li>Masukkan Kode Otentikasi Token.</li>
            <li>Pembayaran Anda telah berhasil.</li>
            </ul>',
            'created_at' => date('Y-m-d H:i:s')
        ]);


        //BRIN
        DB::table('pay_ways')->insert([
            'bank_code' => 'BRIN',
            'description' => '<h3>ATM BRI</h3>
            <ul>
            <li>Input kartu ATM dan PIN Anda</li>
            <li>Pilih Menu Transaksi Lain</li>
            <li>Pilih Menu Pembayaran</li>
            <li>Pilih Menu Lain-lain</li>
            <li>Pilih Menu BRIVA</li>
            <li>Masukkan Nomor Virtual Account, misal. 88788XXXXXXXXXXX</li>
            <li>Pilih Ya</li>
            <li>Ambil bukti bayar anda</li>
            <li>Selesai</li>
            </ul>
            <h3>Mobile Banking BRI</h3>
            <ul>
            <li>Login BRI Mobile</li>
            <li>Pilih Mobile Banking BRI</li>
            <li>Pilih Menu Pembayaran</li>
            <li>Pilih Menu BRIVA</li>
            <li>Masukkan Nomor Virtual Account, misal. 88788XXXXXXXXXXX</li>
            <li>Masukkan Nominal misal. 10000</li>
            <li>Klik Kirim</li>
            <li>Masukkan PIN Mobile</li>
            <li>Klik Kirim</li>
            <li>Bukti bayar akan dikirim melalui sms</li>
            <li>Selesai</li>
            </ul>
            <h3>Internet Banking BRI</h3>
            <ul>
            <li>Login Internet Banking</li>
            <li>Pilih Pembayaran</li>
            <li>Pilih BRIVA</li>
            <li>Masukkan Nomor Virtual Account, misal. 88788XXXXXXXXXXX</li>
            <li>Klik Kirim</li>
            <li>Masukkan Password</li>
            <li>Masukkan mToken</li>
            <li>Klik Kirim</li>
            <li>Bukti bayar akan ditampilkan</li>
            <li>Selesai</li>
            </ul>',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        //CENA
        DB::table('pay_ways')->insert([
            'bank_code' => 'CENA',
            'description' => '<h3>ATM BCA</h3>
            <ul>
            <li>Input kartu ATM dan PIN Anda</li>
            <li>Pilih Menu Transaksi Lainnya</li>
            <li>Pilih Transfer</li>
            <li>Pilih Ke rekening BCA Virtual Account</li>
            <li>Input Nomor Virtual Account, misal. 123456789012XXXX</li>
            <li>Pilih Benar</li>
            <li>Pilih Ya</li>
            <li>Ambil bukti bayar Anda</li>
            <li>Selesai</li>
            </ul>
            <h3>Mobile Banking BCA</h3>
            <ul>
            <li>Login Mobile Banking</li>
            <li>Pilih m-Transfer</li>
            <li>Pilih BCA Virtual Account</li>
            <li>Input Nomor Virtual Account, misal. 123456789012XXXX sebagai No. Virtual Account</li>
            <li>Klik Send</li>
            <li>Informasi Virtual Account akan ditampilkan</li>
            <li>Klik OK</li>
            <li>Input PIN Mobile Banking</li>
            <li>Bukti bayar ditampilkan</li>
            <li>Selesai</li>
            </ul>
            <h3>Internet Banking BCA</h3>
            <ul>
            <li>Login Internet Banking</li>
            <li>Pilih Transaksi Dana</li>
            <li>Pilih Transfer Ke BCA Virtual Account</li>
            <li>Input Nomor Virtual Account, misal. 123456789012XXXX sebagai No. Virtual Account</li>
            <li>Klik Kirim</li>
            <li>Input Respon KeyBCA Appli 1</li>
            <li>Klik Kirim</li>
            <li>Bukti bayar ditampilkan</li>
            <li>Selesai</li>
            </ul>',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        //BNIA
        DB::table('pay_ways')->insert([
            'bank_code' => 'BNIA',
            'description' => '<h3>ATM CIMB Niaga</h3>
            <ul>
            <li>Input kartu ATM dan PIN Anda</li>
            <li>Pilih Menu Pembayaran</li>
            <li>Pilih Menu Lanjut</li>
            <li>Pilih Menu Virtual Account</li>
            <li>Masukkan Nomor Virtual Account, misal. 5919XXXXXXXXXXXX</li>
            <li>Pilih Proses</li>
            <li>Data Virtual Account akan ditampilkan</li>
            <li>Pilih Proses</li>
            <li>Ambil bukti bayar anda</li>
            <li>Selesai</li>
            </ul>
            <h3>Mobile Banking CIMB Niaga</h3>
            <ul>
            <li>Login Go Mobile</li>
            <li>Pilih Menu Transfer</li>
            <li>Pilih Menu Transfer ke CIMB Niaga Lain</li>
            <li>Pilih Sumber Dana yang akan digunakan</li>
            <li>Masukkan Nomor Virtual Account, misal. 5919XXXXXXXXXXXX</li>
            <li>Masukkan Nominal misal. 10000</li>
            <li>Klik Lanjut</li>
            <li>Data Virtual Account akan ditampilkan</li>
            <li>Masukkan PIN Mobile</li>
            <li>Klik Konfirmasi</li>
            <li>Bukti bayar akan dikirim melalui sms</li>
            <li>Selesai</li>
            </ul>
            <h3>Internet Banking CIMB Niaga</h3>
            <ul>
            <li>Login Internet Banking</li>
            <li>Pilih Pembayaran Tagihan</li>
            <li>Rekening Sumber - Pilih yang akan Anda digunakan</li>
            <li>Jenis Pembayaran - Pilih Virtual Account</li>
            <li>Untuk Pembayaran - Pilih Masukkan Nomor Virtual Account</li>
            <li>Nomor Rekening Virtual, misal. 5919XXXXXXXXXXXX</li>
            <li>Isi Remark Jika diperlukan</li>
            <li>Klik Lanjut</li>
            <li>Data Virtual Account akan ditampilkan</li>
            <li>Masukkan mPIN</li>
            <li>Klik Kirim</li>
            <li>Bukti bayar akan ditampilkan</li>
            <li>Selesai</li>
            </ul>',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        //BDIN
        DB::table('pay_ways')->insert([
            'bank_code' => 'BDIN',
            'description' => '<h3>ATM Danamon</h3>
            <ul>
            <li>Input PIN ATM Anda</li>
            <li>Pilih Menu Pembayaran >>> Virtual Account Masukan nomor Virtual Account</li>
            <li>Masukkan Nominal</li>
            <li>Pada layar konfirmasi pembayaran, pastikan transaksi sudah benar -> pilih Ya untuk memproses transaksi</li>
            </ul>
            <h3>Aplikasi D-Mobile Danamon</h3>
            <ul>
            <li>Login pada Aplikasi D-Mobile</li>
            <li>Pilih menu Virtual Account</li>
            <li>Masukan 16 digit nomor virtual account</li>
            <li>Masukan Nominal</li>
            <li>Pada layar konfirmasi pembayaran, pastikan transaksi sudah benar -> pilih Ya untuk memproses transaksi.</li>
            </ul>',
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
