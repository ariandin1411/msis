<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),
            'first_name' => Str::random(10),
            'mid_name' => Str::random(10),
            'last_name' => Str::random(10),
            'nig' => random_int(10000000, 99999999),
            'nis' => 0,
            'type' => 0,
            'gender' => 'M',
            'address' => Str::random(100),
            'phone_number' => '081219836581',
            'year_of_entry' => date('Y-m-d'),
            'active' => 1,
            'insert_by' => 1,
            'birth_date' => date('Y-m-d'),
            'born_at' => 'Bogor',
            'religion' => 'islam'
        ]);
    }
}
