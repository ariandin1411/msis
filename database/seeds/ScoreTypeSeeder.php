<?php

use Illuminate\Database\Seeder;

class ScoreTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('score_types')->insert([
            'name' => 'UAS',
            'weight' => 50,
            'active' => 1,
            'company_id' => 1,
            'insert_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('score_types')->insert([
            'name' => 'UTS',
            'weight' => 50,
            'active' => 1,
            'company_id' => 1,
            'insert_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
