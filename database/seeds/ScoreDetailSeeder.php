<?php

use Illuminate\Database\Seeder;

class ScoreDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('score_details')->insert([
            'score_id' => 1,
            'score_type_id' => 1,
            'score' => 100,
            'active' => 1,
            'insert_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('score_details')->insert([
            'score_id' => 1,
            'score_type_id' => 2,
            'score' => 100,
            'active' => 1,
            'insert_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
