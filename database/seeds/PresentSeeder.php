<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PresentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('presents')->insert([
            'user_id'=>1,
            'class_id'=>1,
            'desc'=>'present',
            'year_id'=>1,
            'semester'=>1,
            'company_id'=>1,
            'active'=>1,
            'insert_by'=>1,
            'date_in'=> Carbon::parse('2020-07-21'),

        ]);
    }
}
