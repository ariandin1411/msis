<?php

use Illuminate\Database\Seeder;

class UserGuardianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_guardians')->insert([
            'student_user_id'=>1,
            'guardian_user_id'=>3,
            'company_id' => 1,
            'active'=> 1,
            'insert_by' => 1,
        ]);

        DB::table('user_guardians')->insert([
            'student_user_id'=>2,
            'guardian_user_id'=>3,
            'company_id' => 1,
            'active'=> 1,
            'insert_by' => 1,
        ]);
    }
}
