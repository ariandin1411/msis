<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
          'user_id' => 1,
          'role_id' => 1,
          'active' => 1,
          'insert_by' => 1,
          'created_at' => date('Y-m-d H:i:s')
      ]);
    }
}
