<?php

use Illuminate\Database\Seeder;

class RoleNextSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'role_name' => 'Guardian',
            'level' => 2,
            'description' => 'Guardian',
            'active' => 1,
            'insert_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
           DB::table('roles')->insert([
             'role_name' => 'Teacher',
             'level' => 3,
             'description' => 'Teacher',
             'active' => 1,
             'insert_by' => 1,
             'created_at' => date('Y-m-d H:i:s')
        ]);
          DB::table('roles')->insert([
            'role_name' => 'Student',
            'level' => 4,
            'description' => 'Student',
            'active' => 1,
            'insert_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
         ]);
    }
}
