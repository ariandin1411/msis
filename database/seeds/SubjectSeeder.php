<?php

use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert([
            'subject_name' => 'Matematika',
            'active' => 1,
            'company_id' => 1,
            'insert_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
