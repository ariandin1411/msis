<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\UserRole;
use App\Models\UserCompany;
use App\Models\UserClass;
use App\Models\StudentGuardian;
use App\User;

class ImportStudentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        $file = __DIR__.'/../../resources/csv/import/importstudentsmsissample.csv';
        $header = ['first_name','mid_name','last_name','email','email_verified_at','password','remember_token','nig','nis','type','gender','address','phone_number','phone_number2','year_of_entry','birth_date','born_at','religion','nisn','profile_pic','active','insert_by','update_by','company_id','class_id','guardian_name','guardian_phone_number','guardian_email'];
        $data = $this->csv_to_array($file, $header);
        $users = array_map(function ($arr) use ($now) {
            unset($arr['company_id'], $arr['class_id'], $arr['guardian_name'], $arr['guardian_phone_number'], $arr['guardian_email']);
            return $arr + ['created_at' => $now, 'updated_at' => $now];
        }, $data);

        $class = array_map(function ($arr) use ($now) {
            return $arr['class_id'];
        }, $data);

        $guardian = array_map(function ($arr) use ($now) {
            return ['guardian_name'=> $arr['guardian_name'],'guardian_phone_number'=> $arr['guardian_phone_number'],'guardian_email'=> $arr['guardian_email']];
        }, $data);

        $company_id = $data[0]['company_id'];
        DB::beginTransaction();
        
        try {
            foreach ($users as $key => $value) {
                DB::table('users')->insert($value);

                $insert_user = User::where('email', $value['email'])->first();
                
                //Insert User Company
                $userCompany = new UserCompany();
                $userCompany->user_id = $insert_user['id'];
                $userCompany->company_id = $company_id;
                $userCompany->active = 1;
                $userCompany->insert_by = 1;
                $userCompany->save();

                //Insert User Role
                $userRole = new UserRole();
                $userRole->user_id =  $insert_user['id'];
                $userRole->role_id = 4; // this role for student
                $userRole->active = 1;
                $userRole->insert_by = 1;
                $userRole->save();

                //Insert User Class
                $userClass = new UserClass();
                $userClass->user_id = $insert_user['id'];
                $userClass->class_id = $class[$key];
                $userClass->active = 1;
                $userClass->insert_by = 1;
                $userClass->save();

                //Insert Student Guardian
                $studentGuardian = new StudentGuardian();
                $studentGuardian->user_id = $insert_user['id'];
                $studentGuardian->name = $guardian[$key]['guardian_name'];
                $studentGuardian->phone_number = $guardian[$key]['guardian_phone_number'];
                $studentGuardian->email = $guardian[$key]['guardian_email'];
                $studentGuardian->active = 1;
                $studentGuardian->insert_by = 1;
                $studentGuardian->save();
            }
            DB::commit();
        } catch (\Exception $e) {
            echo $e;
            DB::rollback();
        }
    }

    public function csv_to_array($filename, $header)
    {
        $delimiter = ',';
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }

        $data = [];
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
}
