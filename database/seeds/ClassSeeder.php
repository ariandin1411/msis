<?php

use Illuminate\Database\Seeder;

class ClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classes')->insert([
            'class_name'=>'class dev 1',
            'class_sub_name'=>'sub class dev 1',
            'major'=>'science',
            'active'=>1,
            'company_id'=>1,
            'insert_by'=>1,
        ]);
    }
}
