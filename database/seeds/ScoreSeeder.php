<?php

use Illuminate\Database\Seeder;

class ScoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('scores')->insert([
            'user_id' => 1,
            'subject_id' => 1,
            'teacher_id' => 1,
            'class_id' => 1,
            'score_total' => 100,
            'active' => 1,
            'company_id' => 1,
            'year_id' => 1,
            'semester' => 1,
            'insert_by' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
