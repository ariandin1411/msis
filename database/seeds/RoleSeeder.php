<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
          'role_name' => 'admin',
          'level' => 1,
          'description' => 'Admin super user',
          'active' => 1,
          'company_id' => 1,
          'insert_by' => 1,
          'created_at' => date('Y-m-d H:i:s')
      ]);
    }
}
