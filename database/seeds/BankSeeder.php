<?php

use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->insert([
            'bank_code' => 'BMRI',
            'bank_name' => 'Bank Mandiri',
            'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('banks')->insert([
            'bank_code' => 'BNIN',
            'bank_name' => 'BNI 46',
            'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('banks')->insert([
            'bank_code' => 'BRIN',
            'bank_name' => 'BRI',
            'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('banks')->insert([
            'bank_code' => 'CENA',
            'bank_name' => 'BCA',
            'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('banks')->insert([
            'bank_code' => 'BBBA',
            'bank_name' => 'Bank Permata',
            'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('banks')->insert([
            'bank_code' => 'BNIA',
            'bank_name' => 'Bank CIMB Niaga',
            'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('banks')->insert([
            'bank_code' => 'BDIN',
            'bank_name' => 'Bank Danamon',
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
