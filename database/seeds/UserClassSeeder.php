<?php

use Illuminate\Database\Seeder;

class UserClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_classes')->insert([
            'user_id'=>1,
            'class_id'=>1,
            'active'=> 1,
            'insert_by' => 1,
        ]);
    }
}
