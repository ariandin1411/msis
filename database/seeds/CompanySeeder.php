<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'company_number'=>'0',
            'company_name'=>'a',
            'website'=>'a.com',
            'email'=>'a@gmail.com',
            'phone1'=>'123456',
            'phone2'=>'123456',
            'phone3'=>'123456',
            'active'=> 1,
            'province'=>'Banten',
            'city'=>'Tangerang',
            'district'=>'BSD',
            'address'=>'BSD',
            'logo'=>'BSD',
            'classification'=>'IT',
            'bank_account_name'=>'A',
            'bank_name'=>'BCA',
            'bank_account'=>'123456',
            'insert_by'=>1,
        ]);
    }
}
