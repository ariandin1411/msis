<?php

use Illuminate\Database\Seeder;
use App\User;

class StudentFactorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(\App\User::class, 5)->make();

        foreach ($users as $key => $value) {
            DB::table('users')->insert([
                'email' => $value->email,
                'password' => $value->password,
                'first_name' => $value->first_name,
                'mid_name' => $value->mid_name,
                'last_name' => $value->last_name,
                'nig' => $value->nig,
                'nis' => $value->nis,
                'type' => $value->type,
                'gender' => $value->gender,
                'address' => $value->address,
                'phone_number' => $value->phone_number,
                'year_of_entry' => $value->year_of_entry,
                'active' => $value->active,
                'insert_by' => $value->insert_by,
                'birth_date' => $value->birth_date,
                'born_at' => $value->born_at,
                'religion' => $value->religion
            ]);

            $user_id = User::select('id')->where('email', $value->email)->first();

            DB::table('user_companies')->insert([
                'user_id'=> $user_id->id,
                'company_id'=>1,
                'active'=> 1,
                'insert_by' => 1,
            ]);
            
            DB::table('user_roles')->insert([
                'user_id' =>  $user_id->id,
                'role_id' => 4,
                'active' => 1,
                'insert_by' => 1,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            DB::table('student_guardians')->insert([
                'user_id'=> $user_id->id,
                'name'=>'nama wali',
                'phone_number'=>'1234567',
                'email'=>'guardian@gmail.com',
                'active'=> 1,
                'insert_by' => 1,
                ]);

             DB::table('user_classes')->insert([
                'user_id'=> $user_id->id,
                'class_id'=>1,
                'year_id'=>1,
                'active'=> 1,
                'insert_by' => 1,
        ]);

        }
    }
}
