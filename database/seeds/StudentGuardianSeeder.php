<?php

use Illuminate\Database\Seeder;

class StudentGuardianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('student_guardians')->insert([
        'user_id'=>1,
        'name'=>'nama wali',
        'phone_number'=>'1234567',
        'email'=>'guardian@gmail.com',
        'active'=> 1,
        'insert_by' => 1,
        ]);
    }
}
