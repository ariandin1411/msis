<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserCompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_companies')->insert([
            'user_id'=>1,
            'company_id'=>1,
            'active'=> 1,
            'insert_by' => 1,
        ]);
    }
}
