<?php

use Illuminate\Database\Seeder;

class YearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('years')->insert([
            'year_name'=>'2020 / 2021',
            'active'=>1,
            'insert_by'=>1,
        ]);
    }
}
