<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
////    return view('welcome');
//
//});
Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/upload', 'HomeController@upload')->name('upload');
Route::post('/upload', 'HomeController@storeUpload')->name('store-upload');
Route::get('/get-classes', 'HomeController@getClasses')->name('get-classes');
Route::get('/kelas', 'Api\Classes\ClassesController@view')->name('kelas');
Route::get('/kelas-export-excel', 'Api\Classes\ClassesController@exportExcel');
Route::get('/present-export-excel/{companyId}/{classId}/{reqDate}', 'Api\Present\PresentController@exportExcel');
Route::get('/present-summary-export-excel/{classId}/{comapanyId}/{yearId}', 'Api\Present\PresentController@exportExcelSummary');
Route::get('/invoices-export-excel/{companyId}/{fromDate}/{toDate}', 'Api\Invoice\InvoiceController@exportExcel');
Route::get('/score-export-excel/{companyId}/{classId}/{subjectId}', 'Api\Score\ScoreController@exportExcel');
Route::get('mail/send-va-created', 'MailController@send')->name('end-va-created');

Route::get('user-pdf-view', 'API\UserController@userPdfView')->name('user-pdf-view');
Route::get('user-excel-view', 'API\UserController@userExcelView')->name('user-excel-view');
Route::get('students-excel-view', 'API\UserController@studentsExcelView')->name('user-excel-view');
Route::get('teacherclasses-excel-view', 'API\TeacherClassesController@teacherClassesExcelView')->name('teacherclasses-excel-view');

Route::get('invoice', function(){
  return view('invoice');
});

Route::get('{path}', 'HomeController@index')->where('path', '([A-z\d-\/_.]+)?');


