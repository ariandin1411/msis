<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Api\Auth\AuthController@index')->name('login');
Route::post('reset-password', 'Api\Auth\AuthController@resetPassword')->name('resetPassword');
Route::apiResources(['users' => 'Api\User\UserController']);
Route::put('users/{id}', 'Api\User\UserController@update');
Route::put('change-classes', 'Api\User\UserController@changeClasses');

Route::get('schedules/getCompanyClass', 'Api\Schedule\ScheduleController@getDayByCompanyClasses');
Route::get('invoices/get_inv_by_comp_id', 'Api\Invoice\InvoiceController@getInvByCompId');
Route::get('classes/view', 'Api\Classes\ClassesController@view');
Route::get('invoices/get_per_date', 'Api\Invoice\InvoiceController@getInvList');

Route::apiResources(['companies' => 'Api\Company\CompanyController']);
Route::apiResources(['classes' => 'Api\Classes\ClassesController']);
Route::apiResources(['subjects' => 'Api\Subject\SubjectController']);
Route::apiResources(['lesson-hours' => 'Api\LessonHour\LessonHourController']);
Route::apiResources(['schedules' => 'Api\Schedule\ScheduleController']);
Route::apiResources(['products' => 'Api\Product\ProductController']);
Route::get('provinces', 'Api\Company\CompanyController@province');
Route::get('cities/{id}', 'Api\Company\CompanyController@city');
Route::get('districts/{id}', 'Api\Company\CompanyController@district');
Route::apiResources(['presents' => 'Api\Present\PresentController']);
Route::get('presents-list','Api\Present\PresentController@presentsList');
Route::get('presents-summary','Api\Present\PresentController@presentsSummary');
Route::apiResources(['years' => 'Api\Year\YearController']);
Route::apiResources(['scores' => 'Api\Score\ScoreController']);
Route::apiResources(['roles' => 'Api\Role\RoleController']);
Route::apiResources(['scoreType' => 'Api\Score\ScoreTypeController']);
Route::put('scoreType/{id}', 'Api\Score\ScoreTypeController@update');
Route::post('payments/noti','Api\Payment\PaymentController@noti')->name('db-process-url');
Route::get('payments/payment-pg','Api\Payment\PaymentController@paymentPg')->name('payment-pg');
Route::apiResources(['payments' => 'Api\Payment\PaymentController']);
Route::apiResources(['invoices' => 'Api\Invoice\InvoiceController']);
Route::get('invoice-by-id/{id}','Api\Invoice\InvoiceController@showInvoice');
Route::apiResources(['notes' => 'Api\Note\NoteController']);
Route::apiResources(['user-guardian' => 'Api\User\UserGuardianController']);

// installment custom
Route::get('/installments/get-loan-data','Api\Installment\InstallmentController@showByUserIdandProdId');
Route::apiResources(['installments' => 'Api\Installment\InstallmentController']);
