<?php

namespace App\Imports;

use App\Models\StudentGuardian;
use App\Models\UserClass;
use App\Models\UserCompany;
use App\Models\UserRole;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;

class StudentsImport implements ToCollection
{
    protected $_classes = null;
    protected $_company = null;
    public function __construct($classes, $company)
    {
      $this->_classes = $classes;
      $this->_company = $company;
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
      foreach ($rows as $key => $row)
      {
        if ($key > 0 && isset($row[0]) && $row[0] != '') {
          // echo $row[0];die;
          $input['first_name'] = trim($row[0]);
          $input['mid_name'] = trim($row[1]);
          $input['last_name'] = trim($row[2]);
          $input['email'] = trim($row[3]);
          $input['nis'] = trim($row[4]);
          $input['phone_number'] = trim($row[5]);
          $input['born_at'] = trim($row[8]);
          $input['birth_date'] = trim($row[9]);
          $input['year_of_entry'] = trim($row[10]);
          $input['religion'] = strtolower(trim($row[11]));
          $input['gender'] = strtoupper(trim($row[12]));
          $input['address'] = 'Tangerang';
          $input['active'] = 1;
          $input['insert_by'] = 1;
          $input['password'] =  Hash::make('123456');
//          dd($input);
          $user = User::create($input);
          if ($user) {
            $userCompany = new UserCompany();
            $userCompany->user_id = $user->id;
            $userCompany->company_id = $this->_company;
            $userCompany->active = 1;
            $userCompany->insert_by = $user->id;
            $userCompany->save();

            $userRole = new UserRole();
            $userRole->user_id = $user->id;
            $userRole->role_id = 4; // this role for student
            $userRole->active = 1;
            $userRole->insert_by = $user->id;
            $userRole->save();

            //Insert Student Guardian
            $studentGuardian = new StudentGuardian();
            $studentGuardian->user_id = $user->id;
            $studentGuardian->name = 'Test Guardian';
            $studentGuardian->phone_number = '081219836581';
            $studentGuardian->email = 'db_duabelas@yahoo.com';
            $studentGuardian->active = 1;
            $studentGuardian->insert_by = $user->id;
            $studentGuardian->save();

            //Insert User Class
            $userClass = new UserClass();
            $userClass->user_id = $user->id;
            $userClass->class_id = $this->_classes;
            $userClass->active = 1;
            $userClass->insert_by = $user->id;
            $userClass->save();
          }
        }
      }

      return true;
    }
}
