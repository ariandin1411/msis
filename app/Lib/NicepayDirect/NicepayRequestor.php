<?php

namespace App\Lib\NicepayDirect;

/*
 * ____________________________________________________________
 *
 * Copyright (C) 2016 NICE IT&T
 *
 * Please do not modify this module.
 * This module may used as it is, there is no warranty.
 *
 * @ description : PHP SSL Client module.
 * @ name        : NicepayLite.php
 * @ author      : NICEPAY I&T (tech@nicepay.co.kr)
 * @ date        :
 * @ modify      : 22.02.2016
 *
 * 2016.02.22 Update Log
 * Please contact it.support@ionpay.net for inquiry
 *
 * ____________________________________________________________
 */

include_once ('NicepayLogger.php');
include_once ('NicepayConfig.php');

class NicepayRequestor {
    public $sock = 0;
    public $apiUrl;
    public $port = 443;
    public $status;
    public $headers = "";
    public $body = "";
    public $request;
    public $errorcode;
    public $errormsg;
    public $log;
    public $timeout;

    public function __construct() {
        $this->log = new NicepayLogger();
    }

    public function operation($type) {
        if ($type == "requestVA") {
            $this->apiUrl = NICEPAY_REQ_VA_URL;
        } else if ($type == "threeDSecure") {
            $this->apiUrl = NICEPAY_3DSECURE_URL;
        } else if ($type == "reqChargeCard") {
            $this->apiUrl = NICEPAY_REQ_CHARGE_URL;
        } else if ($type == "chargeCard") {
            $this->apiUrl = NICEPAY_CHARGE_URL;
        } else if ($type == "cancel") {
            $this->apiUrl = NICEPAY_CANCEL_URL;
        } else if ($type == "checkPaymentStatus") {
            $this->apiUrl = NICEPAY_ORDER_STATUS_URL;
        } else if ($type == "recurringToken") {
            $this->apiUrl = NICEPAY_RECURRING_TOKEN;
        } else if ($type == "requestCVS") {
            $this->apiUrl = NICEPAY_REQ_CVS_URL;
        }
    }

    public function apiRequest($data, $apiUrl) {

        $postdata = json_encode($data);
        // print_r($postdata);exit;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $server_result = curl_exec ($ch);
        // print_r($server_result);exit;
        $result = json_decode($server_result);

        curl_close($ch);

        return $result;
    }
}
