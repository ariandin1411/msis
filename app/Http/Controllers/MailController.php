<?php
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use App\Mail\VaCreated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
 
class MailController extends Controller
{
    public function send(Request $request)
    {
        $objDemo = new \stdClass();
        $objDemo->demo_one = 'Demo One Value';
        $objDemo->demo_two = 'Demo Two Value';
        $objDemo->sender = 'SenderUserName';
        $objDemo->receiver = 'ReceiverUserName';
 
        Mail::to("db_duabelas@yahoo.com")->send(new VaCreated($objDemo));
    }
}
