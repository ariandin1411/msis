<?php

namespace App\Http\Controllers\Api\Score;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Score;
use App\Models\ScoreDetail;
use App\Models\ScoreType;
use DB;
use Illuminate\Support\Facades\Log;
use App\Exports\ScoresExport;
use Excel;

class ScoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['exportExcel']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $score = Score::where('active', 1);

        if ($request->has('user_company_id')) {
            if ($request->user_company_id != 2) {
                $score = $score->where('company_id', $request->user_company_id);
            }
        }

        if ($request->has('user_id')) {
            $score = $score->where('user_id', $request->user_id);
        }

        if ($request->has('teacher_id')) {
            $score = $score->where('teacher_id', $request->teacher_id);
        }

        if ($request->has('subject_id')) {
            $score = $score->where('subject_id', $request->subject_id);
        }

        if ($request->has('class_id')) {
            $score = $score->where('class_id', $request->class_id);
        }

        if ($request->has('semester')) {
          $score = $score->where('semester', $request->semester);
        }

        if ($request->has('year_id')) {
          $score = $score->where('year_id', $request->year_id);
        }

        $score = $score->get();

        foreach ($score as $key => $value) {
            $value->company = $value->company;
            $value->user = $value->user;
            $value->teacher = $value->teacher;
            $value->subject = $value->subject;
            $value->classes = $value->classes;
            $value->score_detail = $value->score_detail;
        }

        return response()->json($score, 200);
    }

    //
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => ['required'],
            'student_user_id' => ['required'],
            'subject_id' => ['required'],
            'teacher_id' => ['required'],
            'class_id' => ['required'],

            'year_id' => ['required'],
            'semester' => ['required'],
            'company_id' => ['required'],
            'score_type_id' => ['required'],
            'score' => ['required'],

        ]);

        DB::beginTransaction();

        try {
            $index=0;
            foreach ($request->student_user_id as $key => $value) {
                // $input = $request->all();
                // $input['active'] = 1;
                // $input['insert_by'] = $request->user_id;

                $score = Score::where('active', 1)
            ->where('user_id', $value)
            ->where('subject_id', $request->subject_id)
            ->where('teacher_id', $request->teacher_id)
            ->where('class_id', $request->class_id)
            ->where('year_id', $request->year_id)
            ->where('company_id', $request->company_id)->first();
                // print_r($score);die;

                //Count score_total
                // $scoreType = ScoreType::find($request->score_type_id)->first();
                $scoreType = ScoreType::where('id', $request->score_type_id)->first();
                $scoreRequest = $request->score[$index];
                $weight = $scoreType->weight;
                $AddScoreTotal = $scoreRequest *  $weight /100;

                if ($score) {
                    //update score
                    $scoreDetail = ScoreDetail::where('active', 1)
                    ->where('score_id', $score->id)
                    ->where('score_type_id', $request->score_type_id)->first();

                    if ($scoreDetail) {
                        $existingScoreDetail = $scoreDetail->score *  $weight /100;
                        $updateScore_total = $score->score_total - $existingScoreDetail +  $AddScoreTotal;
                    } else {
                        $updateScore_total = $score->score_total +  $AddScoreTotal;
                    }



                    $score->score_total = $updateScore_total;
                    $score->save();
                } else {
                    //insert new score
                    $newScore_total = $AddScoreTotal;

                    $inputScore['user_id'] = $request->student_user_id[$index];
                    $inputScore['subject_id'] =  $request->subject_id;
                    $inputScore['teacher_id'] = $request->teacher_id;
                    $inputScore['class_id'] = $request->class_id;
                    $inputScore['score_total'] = $newScore_total;
                    $inputScore['year_id'] = $request->year_id;
                    $inputScore['semester'] = $request->semester;
                    $inputScore['company_id'] = $request->company_id;
                    $inputScore['active'] = 1;
                    $inputScore['insert_by'] = $request->user_id;

                    $score = Score::create($inputScore);
                }

                if ($score) {
                    $inputDetail['score_type_id'] = $request->score_type_id;
                    $inputDetail['score'] = $request->score[$index];
                    $inputDetail['score_id'] = $score->id;
                    $inputDetail['active'] = 1;
                    $inputDetail['insert_by'] = $request->user_id;

                    // print_r($inputDetail);die;
                    $scoreDetail = ScoreDetail::where('active', 1)
                    ->where('score_id', $inputDetail['score_id'])
                    ->where('score_type_id', $inputDetail['score_type_id'])->first();

                    // print_r($scoreDetail);die;
                    if ($scoreDetail) {
                        //update scoreDetail
                        $scoreDetail->score = $inputDetail['score'];
                        $scoreDetail->save();
                    } else {
                        //insert new scoreDetail
                        $scoreDetail = ScoreDetail::create($inputDetail);
                    }

                    // print_r($scoreDetail);
                }


                $index++;
            }
            DB::commit();
            $success['result'] = 'Success';
            $success['code'] = '00';

            return response()->json($success, 200);
        } catch (\Exception $e) {
            DB::rollback();
            // print_r($e->getMessage());die;
            // return ['result' => $e, 'code' => '01'];
            $message = ['msg' => $e->getMessage(), 'code' => '0500'];
            return response()->json($message, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function exportExcel(Request $request, $companyId, $classId, $subjectId)
    {
        Log::info('Start score exportExcel');
        Log::info('company_id is : '.$companyId);
        Log::info('classId is : '.$classId);
        Log::info('subjectId is : '.$subjectId);

        $scores = Score::query();

        $scores = $scores->where('company_id', $companyId)
        ->where('class_id', $classId)
        ->where('subject_id', $subjectId);

        if ($request->has('semester')) {
          $scores = $scores->where('semester', $request->semester);
        }

        if ($request->has('year_id')) {
          $scores = $scores->where('year_id', $request->year_id);
        }

        Log::info('query is : '.$scores->toSql());
        $scores = $scores->orderBy('id')->get();
        $header[] = array();
        $header3[] = array("","","Nilai");
        $header4[] = array();

        $header4[0][0] = "No";
        $header4[0][1] = "Nama";
        $header4[0][2] = "Total Nilai";
        $index=3;

        foreach ($scores as $key=>$val) {
            $val->score_detail = $val->score_detail;

            foreach ($val->score_detail as $key=>$val) {
                if (!in_array($val->score_type->name, $header4[0])) {
                    array_push($header4[0], $val->score_type->name);
                }
            }
        }


        $class_name = "";
        $subject_name = "";
        $i=1;

        foreach ($scores as $key=>$val) {
            $val->score_detail = $val->score_detail;
            $val->user = $val->user;
            $val->classes = $val->classes;
            $val->subject = $val->subject;

            $class_name = $val->classes->class_name." - ".$val->classes->class_sub_name;
            if ($val->classes->major) {
              $class_name = $val->classes->class_name.$val->classes->major." - ".$val->classes->class_sub_name;
            }
            $subject_name =  $val->subject->subject_name;

            $header4[$i]["No"] = $i;
            $header4[$i]["Nama"] = $val->user->first_name." ".$val->user->mid_name." ".$val->user->last_name;
            $header4[$i]["Total Nilai"] = $val->score_total;
            foreach ($val->score_detail as $key=>$valDetail) {
                for ($j=3 ; $j < count($header4[0]); $j++) {
                    if ($header4[0][$j]==$valDetail->score_type->name) {
                        $header4[$i][$valDetail->score_type->name] = $valDetail->score;
                    } else {
                        if (!array_key_exists($header4[0][$j], $header4[$i])) {
                            $header4[$i][$header4[0][$j]] = "0";
                        }
                    }
                }
            }
            $i++;
        }

        $header1[] = array("","Kelas : ",$class_name);
        $header2[] = array("","Mata Pelajaran : ", $subject_name);
        $header = array($header1,$header2,$header3,$header4);

        $export = new ScoresExport($header);
        $nama_file = 'laporan_nilai_'.date('Y-m-d_H-i-s').'.xlsx';
        return Excel::download($export, $nama_file);
    }
}
