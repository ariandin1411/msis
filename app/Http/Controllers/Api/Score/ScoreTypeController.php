<?php

namespace App\Http\Controllers\Api\Score;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ScoreType;

class ScoreTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $scoreType = ScoreType::where('active', 1);

        if ($request->has('user_company_id')) {
            if ($request->user_company_id != 2) {
                $scoreType = $scoreType->where('company_id', $request->user_company_id);
            }
        }

        if ($request->has('name')) {
            $scoreType = $scoreType->where('name', $request->name);
        }

        if ($request->has('weight')) {
            $scoreType = $scoreType->where('weight', $request->weight);
        }
        
        $scoreType = $scoreType->get();


        return response()->json($scoreType, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'name' => ['required', 'string', 'max:191'],
            'weight' => ['required'],
            'company_id' => ['required'],
        ]);

        $input = $request->all();
        $input['active'] = 1;
        $input['insert_by'] = $request->user_id;
        $scoreType = ScoreType::create($input);
        if($scoreType){
            return response()->json($scoreType, 200);
        }

        $message = ['msg' => 'Error when input data', 'code' => '0500'];

        return response()->json($message, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $scoreType = ScoreType::find($id);

        if($scoreType){
            return response()->json($scoreType, 200);
        }

        $message = ['msg' => 'Data not found', 'code' => '0404'];

        return response()->json($message, 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:191'],
            'weight' => ['required']
        ]);

        $scoreType = ScoreType::find($id);
        $input = $request->all();
        $input['update_by'] = $request->user_id;
        if($scoreType){
            $scoreTypeUpdate = $scoreType->update($input);
            if($scoreTypeUpdate){
                $scoreType = ScoreType::find($id);
                return response()->json($scoreType, 200);
            }
        }

        $message = ['msg' => 'Error when update data', 'code' => '0500'];
        return response()->json($message, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $scoreType = ScoreType::find($id);
        if($scoreType){
            $scoreType->update(['active' => 0]);
            if($scoreType){
                $message = ['msg' => 'Delete data success', 'code' => '0000'];
                return response()->json($message, 200);
            }
        }

        $message = ['msg' => 'Error when delete data', 'code' => '0500'];
        return response()->json($message, 500);
    }
}
