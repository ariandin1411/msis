<?php

namespace App\Http\Controllers\Api\Classes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Classes;
use View;
use Excel;
use App\Exports\ClassViewReport;
use App\Exports\ClassCollectionReport;

class ClassesController extends Controller
{

    public function __construct(){
      $this->middleware('auth:api', ['except' => ['view','exportExcel']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $classes = Classes::where('active', 1);

        if( $request->has('user_company_id') ){
            if( $request->user_company_id != 2 ) {
                $classes = $classes->where('company_id', $request->user_company_id);
            }
        }

        $classes = $classes->get();

        foreach ($classes as $key => $value) {
            $value->company = $value->company;
        }

        return response()->json($classes, 200);
    }

    public function view()
    {
        // $datas = Classes::get();
        $datas = ['class_name' => 'class1', 'class_name'  => 'class2'];
        return View::make('kelas',['datas' => $datas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'class_name' => ['required', 'string', 'max:191'],
            'class_sub_name' => ['required', 'string', 'max:191'],
//            'major' => ['required', 'string', 'max:191'],
        ]);

        $classCheck = Classes::where(
          [
            'class_name' => $request->class_name,
            'class_sub_name' => $request->class_sub_name,
            'major' => $request->major,
            'company_id' => $request->company_id
          ]
        )->count();

        if($classCheck > 0){
          $message = ['msg' => 'Classes is double', 'code' => '0500'];
          return response()->json($message, 500);
        }

        $input = $request->all();
        $input['active'] = 1;
        $input['insert_by'] = $request->user_id;
        $classes = Classes::create($input);
        if($classes){
            return response()->json($classes, 200);
        }

        $message = ['msg' => 'Error when input data', 'code' => '0500'];

        return response()->json($message, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $classes = Classes::find($id);

        if($classes){
            return response()->json($classes, 200);
        }

        $message = ['msg' => 'Data not found', 'code' => '0404'];

        return response()->json($message, 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'class_name' => ['required', 'string', 'max:191'],
            'class_sub_name' => ['required', 'string', 'max:191'],
//            'major' => ['required', 'string', 'max:191'],
        ]);

        $classes = Classes::where([
          'class_name' => $request->class_name,
          'class_sub_name' => $request->class_sub_name,
          'major' => $request->major,
          'company_id' => $request->company_id
        ])->first();

        $input = $request->all();
        $input['update_by'] = $request->user_id;
        if($classes){
            $classesUpdate = $classes->update($input);
            if($classesUpdate){
                $classes = Classes::find($id);
                return response()->json($classes, 200);
            }
        }else{
            $classes = Classes::find($id);
            if($classes){
                $classesUpdate = $classes->update($input);
                if($classesUpdate){
                    $classes = Classes::find($id);
                    return response()->json($classes, 200);
                }
            }
        }

        $message = ['msg' => 'Error when update data', 'code' => '0500'];
        return response()->json($message, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $classes = Classes::find($id);
        if($classes){
            $classes->update(['active' => 0]);
            if($classes){
                $message = ['msg' => 'Delete data success', 'code' => '0000'];
                return response()->json($message, 200);
            }
        }

        $message = ['msg' => 'Error when delete data', 'code' => '0500'];
        return response()->json($message, 500);
    }

    public function exportExcel()
    {
        $nama_file = 'laporan_kelas_'.date('Y-m-d_H-i-s').'.xlsx';
        // return Excel::download(new ClassExport, $nama_file);
        return Excel::download(new ClassCollectionReport,  $nama_file);
    }
}
