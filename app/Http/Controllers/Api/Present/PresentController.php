<?php

namespace App\Http\Controllers\Api\Present;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Year;
use App\Models\Classes;
use App\Models\Present;
use DB;
use Excel;
use App\Exports\PresentsExport;
use Illuminate\Support\Facades\Log;
use stdClass;

class PresentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['exportExcel', 'exportExcelSummary']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::select(
            'users.id',
            'users.first_name',
            'users.last_name',
            'users.nis',
            'users.gender',
            'users.email',
            'users.address',
            'users.phone_number',
            'users.year_of_entry',
            'users.created_at',
            'users.birth_date',
            'p.semester',
            'p.year_id',
            'y.year_name',
            'p.date_in',
            'users.born_at',
            'users.religion',
            'users.nisn',
            'p.id AS _id',
            'p.desc',
            'usercompany.company_id',
            DB::raw('if(kelasdet.id is null,currentkelasdet.id,kelasdet.id) as kelas_id'),
            DB::raw('if(kelasdet.class_name is null,currentkelasdet.class_name,kelasdet.class_name) as class_name'),
            DB::raw('if(kelasdet.class_sub_name is null,currentkelasdet.class_sub_name,kelasdet.class_sub_name) as class_sub_name'),
            DB::raw('if(kelasdet.class_name is null,currentkelasdet.class_name,kelasdet.class_name) as class_name'),
            DB::raw('if(kelasdet.major is null,currentkelasdet.major,kelasdet.major) as major')
        )
    ->leftJoin('user_classes AS kelas', 'kelas.user_id', '=', 'users.id')
    ->leftJoin('user_companies AS usercompany', 'usercompany.user_id', '=', 'users.id')
    ->leftJoin('classes AS currentkelasdet', 'kelas.class_id', '=', 'currentkelasdet.id')
    ->where(['users.type' => 'S', 'users.active' => 1]);

        if ($cari = \Request::get('cariKelas')) {
            if ($cari != 0) {
                $users = $users->where('kelasdet.id', $cari);
            }
        }

        if ($user_comapny_id = \Request::get('user_company_id')) {
            $users = $users->where('usercompany.company_id', $user_comapny_id);
        }


        if ($cari = \Request::get('cariSubject')) {
            if ($cari != 0) {
                $users = $users->where('s.subject_id', $cari);
            }
        }

        $semester = 1;
        if ($cari = \Request::get('cariSemester')) {
          if ($cari != 0) {
            $semester = $cari;
          }
        }

        $yearId = '';
        if ($cari = \Request::get('cariTahun')) {
          $yearId = $cari;
        }

        if ($cari = \Request::get('cariTanggal')) {
            if ($cari != 0) {
                $users = $users->leftJoin('presents AS p', function ($join) use ($cari, $semester, $yearId) {
                    $join->on('users.id', '=', 'p.user_id')
                      ->where('p.date_in', $cari)
                      ->where('p.semester', $semester);
                })
            ->leftJoin('years AS y', 'y.id', '=', 'p.year_id', 'y.id', '=', $yearId)
            ->leftJoin('classes AS kelasdet', 'kelas.class_id', '=', 'kelasdet.id');
            }
        } else {
            $sDate = date('Y-m-d');

            $users = $users->leftJoin('presents AS p', function ($join) use ($sDate) {
                $join->on('users.id', '=', 'p.user_id')->where('p.date_in', $sDate);
            })
            ->leftJoin('classes AS kelasdet', 'kelas.class_id', '=', 'kelasdet.id')
            ->leftJoin('years AS y', 'y.id', '=', 'p.year_id');
        }

        if ($cari = \Request::get('cariNis')) {
            $users = $users->where(function ($query) use ($cari) {
                $query->where('users.first_name', 'LIKE', "%$cari%")
                ->orWhere('users.last_name', 'LIKE', "%$cari%")
                ->orWhere('users.nis', 'LIKE', "%$cari%");
            });
        }

        if ($cari = \Request::get('userId')) {
            $users = $users->where('users.id', $cari);
        }

        return $users->get(); // latest()->paginate(50);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            $input = $request->all();
            $input['active'] = 1;
            $input['insert_by'] = $request->user_id;

            $rowInsert = [];

            if ($request->student && is_array($request->student)) {
                foreach ($request->student as $key => $student) {
                    $sqlCek = Present::where(['date_in' => $request->date_in,
                      'user_id' => $student,
                      'semester' => $request->semester,
                      'year_id' => $request->year,
                    ]);
                    $cekData = $sqlCek->count();

                    if ($cekData == 0) {
                        $rowInsert[] = [
                            'user_id' => $student,
                            'class_id' => $request->class_id,
                            'desc' => $request->present[$key],
                            'year_id' => $request->year,
                            'semester' => $request->semester,
                            'company_id' => $request->company_id,
                            'active' => 1,
                            'insert_by' => $request->user_id,
                            'date_in' => $request->date_in,
                            'created_at' => date('Y-m-d H:i:s')
                        ];
                    } else {
                        $sqlCek = $sqlCek->first();
                        $sqlCek->desc = $request->present[$key];
                        $sqlCek->updated_at = date('Y-m-d H:i:s');
                        $sqlCek->update_by = $request->user_id;
                        $sqlCek->save();
                    }
                }
                $present = Present::insert($rowInsert);
            }
            DB::commit();
            return ['result' => 'Success', 'code' => '00'];
        } catch (\Exception $e) {
            DB::rollback();
            return ['result' => $e, 'code' => '01'];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function presentsList(Request $request)
    {
        $dateReq = date('Y-m');
        if ($request->has('dateReq')) {
            $dateReq = $request->dateReq;
        }

        $dateReq2 = date('Y-m', strtotime($dateReq));
        $date = date('t', strtotime($dateReq2));

        $presentsList = $this->index();
        // print_r($presentsList);die;
        $mainData = [];
        foreach ($presentsList as $key => $value) {
            // echo 'nis : '.$value->class_name.' '.$value->class_sub_name.' - '.$value->desc.' '.$value->last_name.'<br />';
            $tanggal = 1;

            $mainData[$key]['nis'] = $value->nis;
            $mainData[$key]['id'] = $value->id;
            $mainData[$key]['first_name'] = $value->first_name;
            $mainData[$key]['last_name'] = $value->last_name;
            $mainData[$key]['company_id'] = $value->company_id;
            $mainData[$key]['class_id'] = $value->kelas_id;
            $mainData[$key]['class_name'] = $value->class_name;
            $mainData[$key]['class_sub_name'] = $value->class_sub_name;
            $mainData[$key]['major'] = $value->major;
            for ($i=0; $i < $date; $i++) {
                $mainData[$key]['dateList'][$i]['dateList'] = date('d', strtotime($dateReq2.'-'.$tanggal));
                $mainData[$key]['dateList'][$i]['fullDateList'] = date('Y-m-d', strtotime($dateReq2.'-'.$tanggal));

                $selPresent = Present::where(['user_id' => $value->id,
                                                'date_in' => $mainData[$key]['dateList'][$i]['fullDateList']])->first();

                $mainData[$key]['dateList'][$i]['studentList'] = $value->id;
                if ($selPresent) {
                    $mainData[$key]['dateList'][$i]['presentsList'] = $selPresent->desc;
                } else {
                    $mainData[$key]['dateList'][$i]['presentsList'] = '-';
                }

                $tanggal++;
            }
            // die;
        }

        // print_r($mainData);
        // die;

        return ['dateList' => $mainData, 'dateName' => date('F Y', strtotime($request->dateReq)), 'dayTotal' => $date];
        // for($i=0; $i < date('d', strtotime()))
    }

    public function exportExcel($companyId, $classId, $reqDate)
    {
        Log::info('Start score exportExcel');
        Log::info('company_id is : '.$companyId);
        Log::info('classId is : '.$classId);

        $users = User::select(
            'users.id',
            'users.first_name',
            'users.mid_name',
            'users.last_name',
            'users.nis',
            'users.gender',
            'users.email',
            'class.class_name',
            'class.class_sub_name',
            'class.major'
        )
        ->leftJoin('user_classes as userclass', 'userclass.user_id', '=', 'users.id')
        ->leftJoin('classes as class', 'class.id', '=', 'userclass.class_id')
        ->leftJoin('user_companies as usercompany', 'usercompany.user_id', '=', 'users.id')
        ->where('usercompany.company_id', $companyId)
        ->where('class.id', $classId);

        $users = $users->orderBy('id')->get();
        // dd($users->toArray());

        $month = date('m', strtotime($reqDate));
        $monthName = date('M', strtotime($reqDate));
        $year = date('Y', strtotime($reqDate));

        $presents = Present::query();

        $presents = $presents->where('company_id', $companyId)
        ->where('class_id', $classId)
        ->whereMonth('date_in', '=', $month)
        ->whereYear('date_in', '=', $year);


        Log::info('query is : '.$presents->toSql());
        $presents = $presents->orderBy('id')->get();

        $number_of_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        // dd($presents->toArray());

        $header[] = array();
        $header3[] = array();

        $header3[0][0] = "No";
        $header3[0][1] = "Nama";
        $index=2;
        for ($i=1 ; $i <=  $number_of_days; $i++) {
            array_push($header3[0], $i);
        }

        $class_name = "";

        $i=1;
        // dd($header3);
        foreach ($users as $key=>$val) {
            $class_name = $val->class_name." ".$val->class_sub_name;

            $header3[$i]["No"] = $i;
            $header3[$i]["Nama"] = $val->first_name." ".$val->mid_name." ".$val->lastname;

            //fill  all days
            for ($j=1 ; $j <=  $number_of_days; $j++) {
                $header3[$i][$j] = "";
            }

            //fill  present
            for ($j=1 ; $j <=  $number_of_days; $j++) {
                foreach ($presents as $key=>$val2) {
                    $date = date('d', strtotime($val2->date_in));

                    if ($val->id==$val2->user_id && $date == $j) {
                        $header3[$i][$j] = $val2->desc;
                    }
                }
            }

            $i++;
        }

        $header1[] = array("","Kelas : ",$class_name);
        $header2[] = array("","Absensi Bulan : ", $monthName." ".$year);
        $header = array($header1,$header2,$header3);

        $export = new PresentsExport($header);
        $nama_file = 'laporan_absensi_'.date('Y-m-d_H-i-s').'.xlsx';
        return Excel::download($export, $nama_file);
    }

    public function presentsSummary(Request $request) {
        $this->validate($request, [
            'class_id' => ['required'],
            'company_id' => ['required'],
            'year_id' => ['required'],
        ]);

        $yearInfo = Year::find($request->year_id);
        // $thnAjaran = $yearInfo->year_name;
        $datemonth = substr($yearInfo->year_name, 0, 4);
        $datemonth2 = substr($yearInfo->year_name, -4);

        $users = User::select(
            'users.id'
            ,'users.first_name'
            ,'users.mid_name'
            ,'users.last_name'
            ,'users.nis'
            ,'classes.class_name'
            ,'classes.class_sub_name'
            ,'classes.major'
            ,'presents.semester'
            ,'years.year_name'
            ,DB::raw('(select concat(\'{"present":\' ,CAST(count(if(p.desc = \'present\', 1, null)) AS CHAR)
                ,\',"not_present":\' ,CAST(count(if(p.desc = \'not present\', 1, null)) AS CHAR)
                ,\',"permission":\' ,CAST(count(if(p.desc = \'permission\', 1, null)) AS CHAR)
                ,\',"sick":\' ,CAST(count(if(p.desc = \'sick\', 1, null)) AS CHAR)
                ,\',"cnt":\' ,CAST(count(p.desc) AS CHAR) ,\'}\')
                from presents p
                where p.user_id = presents.user_id
                and p.class_id = presents.class_id
                and p.company_id = presents.company_id
                and p.year_id = presents.year_id
                and DATE_FORMAT(p.date_in,"%Y-%m") = "' . $datemonth . '-07"
                group by p.user_id) as jul')
            ,DB::raw('(select concat(\'{"present":\' ,CAST(count(if(p.desc = \'present\', 1, null)) AS CHAR)
                ,\',"not_present":\' ,CAST(count(if(p.desc = \'not present\', 1, null)) AS CHAR)
                ,\',"permission":\' ,CAST(count(if(p.desc = \'permission\', 1, null)) AS CHAR)
                ,\',"sick":\' ,CAST(count(if(p.desc = \'sick\', 1, null)) AS CHAR)
                ,\',"cnt":\' ,CAST(count(p.desc) AS CHAR) ,\'}\')
                from presents p
                where p.user_id = presents.user_id
                and p.class_id = presents.class_id
                and p.company_id = presents.company_id
                and p.year_id = presents.year_id
                and DATE_FORMAT(p.date_in,"%Y-%m") = "' . $datemonth . '-08"
                group by p.user_id) as agu')
            ,DB::raw('(select concat(\'{"present":\' ,CAST(count(if(p.desc = \'present\', 1, null)) AS CHAR)
                ,\',"not_present":\' ,CAST(count(if(p.desc = \'not present\', 1, null)) AS CHAR)
                ,\',"permission":\' ,CAST(count(if(p.desc = \'permission\', 1, null)) AS CHAR)
                ,\',"sick":\' ,CAST(count(if(p.desc = \'sick\', 1, null)) AS CHAR)
                ,\',"cnt":\' ,CAST(count(p.desc) AS CHAR) ,\'}\')
                from presents p
                where p.user_id = presents.user_id
                and p.class_id = presents.class_id
                and p.company_id = presents.company_id
                and p.year_id = presents.year_id
                and DATE_FORMAT(p.date_in,"%Y-%m") = "' . $datemonth . '-09"
                group by p.user_id) as sep')
            ,DB::raw('(select concat(\'{"present":\' ,CAST(count(if(p.desc = \'present\', 1, null)) AS CHAR)
                ,\',"not_present":\' ,CAST(count(if(p.desc = \'not present\', 1, null)) AS CHAR)
                ,\',"permission":\' ,CAST(count(if(p.desc = \'permission\', 1, null)) AS CHAR)
                ,\',"sick":\' ,CAST(count(if(p.desc = \'sick\', 1, null)) AS CHAR)
                ,\',"cnt":\' ,CAST(count(p.desc) AS CHAR) ,\'}\')
                from presents p
                where p.user_id = presents.user_id
                and p.class_id = presents.class_id
                and p.company_id = presents.company_id
                and p.year_id = presents.year_id
                and DATE_FORMAT(p.date_in,"%Y-%m") = "' . $datemonth . '-10"
                group by p.user_id) as okt')
            ,DB::raw('(select concat(\'{"present":\' ,CAST(count(if(p.desc = \'present\', 1, null)) AS CHAR)
                ,\',"not_present":\' ,CAST(count(if(p.desc = \'not present\', 1, null)) AS CHAR)
                ,\',"permission":\' ,CAST(count(if(p.desc = \'permission\', 1, null)) AS CHAR)
                ,\',"sick":\' ,CAST(count(if(p.desc = \'sick\', 1, null)) AS CHAR)
                ,\',"cnt":\' ,CAST(count(p.desc) AS CHAR) ,\'}\')
                from presents p
                where p.user_id = presents.user_id
                and p.class_id = presents.class_id
                and p.company_id = presents.company_id
                and p.year_id = presents.year_id
                and DATE_FORMAT(p.date_in,"%Y-%m") = "' . $datemonth . '-11"
                group by p.user_id) as nov')
            ,DB::raw('(select concat(\'{"present":\' ,CAST(count(if(p.desc = \'present\', 1, null)) AS CHAR)
                ,\',"not_present":\' ,CAST(count(if(p.desc = \'not present\', 1, null)) AS CHAR)
                ,\',"permission":\' ,CAST(count(if(p.desc = \'permission\', 1, null)) AS CHAR)
                ,\',"sick":\' ,CAST(count(if(p.desc = \'sick\', 1, null)) AS CHAR)
                ,\',"cnt":\' ,CAST(count(p.desc) AS CHAR) ,\'}\')
                from presents p
                where p.user_id = presents.user_id
                and p.class_id = presents.class_id
                and p.company_id = presents.company_id
                and p.year_id = presents.year_id
                and DATE_FORMAT(p.date_in,"%Y-%m") = "' . $datemonth . '-12"
                group by p.user_id) as des')
            ,DB::raw('(select concat(\'{"present":\' ,CAST(count(if(p.desc = \'present\', 1, null)) AS CHAR)
                ,\',"not_present":\' ,CAST(count(if(p.desc = \'not present\', 1, null)) AS CHAR)
                ,\',"permission":\' ,CAST(count(if(p.desc = \'permission\', 1, null)) AS CHAR)
                ,\',"sick":\' ,CAST(count(if(p.desc = \'sick\', 1, null)) AS CHAR)
                ,\',"cnt":\' ,CAST(count(p.desc) AS CHAR) ,\'}\')
                from presents p
                where p.user_id = presents.user_id
                and p.class_id = presents.class_id
                and p.company_id = presents.company_id
                and p.year_id = presents.year_id
                and DATE_FORMAT(p.date_in,"%Y-%m") = "' . $datemonth2 . '-01"
                group by p.user_id) as jan')
            ,DB::raw('(select concat(\'{"present":\' ,CAST(count(if(p.desc = \'present\', 1, null)) AS CHAR)
                ,\',"not_present":\' ,CAST(count(if(p.desc = \'not present\', 1, null)) AS CHAR)
                ,\',"permission":\' ,CAST(count(if(p.desc = \'permission\', 1, null)) AS CHAR)
                ,\',"sick":\' ,CAST(count(if(p.desc = \'sick\', 1, null)) AS CHAR)
                ,\',"cnt":\' ,CAST(count(p.desc) AS CHAR) ,\'}\')
                from presents p
                where p.user_id = presents.user_id
                and p.class_id = presents.class_id
                and p.company_id = presents.company_id
                and p.year_id = presents.year_id
                and DATE_FORMAT(p.date_in,"%Y-%m") = "' . $datemonth2 . '-02"
                group by p.user_id) as feb')
            ,DB::raw('(select concat(\'{"present":\' ,CAST(count(if(p.desc = \'present\', 1, null)) AS CHAR)
                ,\',"not_present":\' ,CAST(count(if(p.desc = \'not present\', 1, null)) AS CHAR)
                ,\',"permission":\' ,CAST(count(if(p.desc = \'permission\', 1, null)) AS CHAR)
                ,\',"sick":\' ,CAST(count(if(p.desc = \'sick\', 1, null)) AS CHAR)
                ,\',"cnt":\' ,CAST(count(p.desc) AS CHAR) ,\'}\')
                from presents p
                where p.user_id = presents.user_id
                and p.class_id = presents.class_id
                and p.company_id = presents.company_id
                and p.year_id = presents.year_id
                and DATE_FORMAT(p.date_in,"%Y-%m") = "' . $datemonth2 . '-03"
                group by p.user_id) as mar')
            ,DB::raw('(select concat(\'{"present":\' ,CAST(count(if(p.desc = \'present\', 1, null)) AS CHAR)
                ,\',"not_present":\' ,CAST(count(if(p.desc = \'not present\', 1, null)) AS CHAR)
                ,\',"permission":\' ,CAST(count(if(p.desc = \'permission\', 1, null)) AS CHAR)
                ,\',"sick":\' ,CAST(count(if(p.desc = \'sick\', 1, null)) AS CHAR)
                ,\',"cnt":\' ,CAST(count(p.desc) AS CHAR) ,\'}\')
                from presents p
                where p.user_id = presents.user_id
                and p.class_id = presents.class_id
                and p.company_id = presents.company_id
                and p.year_id = presents.year_id
                and DATE_FORMAT(p.date_in,"%Y-%m") = "' . $datemonth2 . '-04"
                group by p.user_id) as apr')
            ,DB::raw('(select concat(\'{"present":\' ,CAST(count(if(p.desc = \'present\', 1, null)) AS CHAR)
                ,\',"not_present":\' ,CAST(count(if(p.desc = \'not present\', 1, null)) AS CHAR)
                ,\',"permission":\' ,CAST(count(if(p.desc = \'permission\', 1, null)) AS CHAR)
                ,\',"sick":\' ,CAST(count(if(p.desc = \'sick\', 1, null)) AS CHAR)
                ,\',"cnt":\' ,CAST(count(p.desc) AS CHAR) ,\'}\')
                from presents p
                where p.user_id = presents.user_id
                and p.class_id = presents.class_id
                and p.company_id = presents.company_id
                and p.year_id = presents.year_id
                and DATE_FORMAT(p.date_in,"%Y-%m") = "' . $datemonth2 . '-05"
                group by p.user_id) as mei')
            ,DB::raw('(select concat(\'{"present":\' ,CAST(count(if(p.desc = \'present\', 1, null)) AS CHAR)
                ,\',"not_present":\' ,CAST(count(if(p.desc = \'not present\', 1, null)) AS CHAR)
                ,\',"permission":\' ,CAST(count(if(p.desc = \'permission\', 1, null)) AS CHAR)
                ,\',"sick":\' ,CAST(count(if(p.desc = \'sick\', 1, null)) AS CHAR)
                ,\',"cnt":\' ,CAST(count(p.desc) AS CHAR) ,\'}\')
                from presents p
                where p.user_id = presents.user_id
                and p.class_id = presents.class_id
                and p.company_id = presents.company_id
                and p.year_id = presents.year_id
                and DATE_FORMAT(p.date_in,"%Y-%m") = "' . $datemonth2 . '-06"
                group by p.user_id) as jun')
        )
        ->leftJoin('user_companies', 'users.id', '=', 'user_companies.user_id')
        ->leftJoin('user_classes', 'users.id', '=', 'user_classes.user_id')
        ->leftJoin('classes', 'user_classes.class_id', '=', 'classes.id')
        ->leftJoin('presents', 'users.id', '=', 'presents.user_id')
        ->leftJoin('years', 'presents.year_id', '=', 'years.id')
        ->where(['users.type' => 'S',
            'users.active' => 1,
            'presents.class_id' => $request->class_id,
            'presents.company_id' => $request->company_id,
            'years.id' => $request->year_id,
            'years.active' => '1'])
        ->orderBy('users.first_name')
        ->groupBy('users.id');

        return $users->get();
    }

    public function exportExcelSummary($classId, $companyId, $yearId)
    {
        Log::info('Start exportExcel');
        Log::info('class_id is : '.$classId);
        Log::info('company_id is : '.$companyId);
        Log::info('year_id is : '.$yearId);

        $myrequest = new \Illuminate\Http\Request();
        $myrequest['class_id'] = $classId;
        $myrequest['company_id'] = $companyId;
        $myrequest['year_id'] = $yearId;

        $presentsSummary = $this->presentsSummary($myrequest);

        $header[] = array("ID",	"NAMA", "NIS", "KELAS", "JURUSAN", "TAHUN AJARAN", "JULI", "AGUSTUS", "SEPTEMBER", "OKTOBER", "NOVEMBER", "DESEMBER", "JANUARI", "FEBRUARI", "MARET", "APRIL", "MEI","JUNI");

        $i=1;
        foreach ($presentsSummary as $key => $val) {
            $header[] = array(
              "ID"=>$val->id,
              "NAMA"=>$val->first_name." ".$val->midlle_name." ".$val->last_name,
              "NIS"=>$val->nis,
              "KELAS"=>$val->class_name." ".$val->class_sub_name,
              "JURUSAN"=>$val->major,
              "TAHUN AJARAN"=>$val->year_name,
              "JULI"=>$val->jul,
              "AGUSTUS"=>$val->agu,
              "SEPTEMBER"=>$val->sep,
              "OKTOBER"=>$val->okt,
              "NOVEMBER"=>$val->nov,
              "DESEMBER"=>$val->des,
              "JANUARI"=>$val->jan,
              "FEBRUARI"=>$val->feb,
              "MARET"=>$val->mar,
              "APRIL"=>$val->apr,
              "MEI"=>$val->mei,
              "JUNI"=>$val->jun
            );
        }

        $export = new PresentsExport($header);
        $nama_file = 'daftar_absensi_'.date('Y-m-d_H-i-s').'.xlsx';
        return Excel::download($export, $nama_file);
    }
}
