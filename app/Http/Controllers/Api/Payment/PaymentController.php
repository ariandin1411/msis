<?php

namespace App\Http\Controllers\Api\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Invoices;
use App\Models\InvoicesDet;
use App\Models\Payment;
use App\Models\PaymentDet;
use App\Models\Year;
use App\User;
use Auth;
use DB;
use App\Lib\NicepayDirect\NicepayLib;
use App\Lib\NicepayRedirect\NicepayLibRedirect;
use App\Mail\VaCreated;
use App\Mail\VaSuccess;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use App\Models\Bank;
use App\Models\PayWay;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $payments = DB::table('invoices as a')
        ->select(
            'a.id',
            'a.invoice_no',
            'a.user_id',
            'c.first_name',
            'c.mid_name',
            'c.last_name',
            DB::raw('CONCAT_WS(" ", c.first_name, c.last_name) AS user_name'),
            'a.total_amt',
            'b.year_name',
            'a.from_dt',
            'a.to_dt',
            'd.bank_cd',
            'classes.id AS class_id',
            'classes.class_name',
            'classes.class_sub_name',
            DB::raw('(CASE
                        WHEN d.pay_method = "02" THEN "Transfer Bank / Virtual Account"
                        WHEN d.pay_method = "03" THEN "Indomaret / Alfamart"
                        WHEN d.pay_method = "03" THEN "E-Wallet / Uang Elektronik (OVO)"
                        ELSE "Other"
                        END) AS pay_method'),
            'd.txid',
            'd.vacct_no',
            'usercompany.company_id',
            'a.created_at'
        )
        ->leftJoin('years as b', 'a.thn_ajaran', '=', 'b.id')
        ->leftJoin('users as c', 'a.user_id', '=', 'c.id')
        ->leftJoin('user_companies AS usercompany', 'usercompany.user_id', '=', 'c.id')
        ->leftJoin('payments as d', 'a.invoice_no', '=', 'd.payment_no')
        ->leftJoin('user_classes AS userclass', 'userclass.user_id', '=', 'a.user_id')
        ->leftJoin('classes AS classes', 'userclass.class_id', '=', 'classes.id')
        // ->where(['a.user_id'=> $request->user_id, 'c.type' => 'S'])
        // ->where(['a.user_id'=> 24, 'c.type' => 'S'])
        ->orderBy('a.from_dt', 'desc');

        if ($request->has('user_company_id')) {
            if ($request->user_company_id != 2) {
                $payments = $payments->where('usercompany.company_id', $request->user_company_id);
            }
        }

        if ($request->has('class_id')) {
            $payments = $payments->where('classes.id', $request->class_id);
        }

        if ($request->has('user_id')) {
            $payments = $payments->where('a.user_id', $request->user_id);
        }

        $payments = $payments->get();
        $ids = [];
        foreach ($payments as $key => $pay) {
          array_push($ids, $pay->id);
        }

        $dets = InvoicesDet::select(
          'invoices_dets.id',
          'invoices_dets.invoice_id',
          'invoices_dets.prod_id',
          'invoices_dets.prod_nm',
          'invoices_dets.sale_prc',
          'p.type'
        )
          ->join('products AS p', 'p.id', '=', 'invoices_dets.prod_id')
          ->whereIn('invoice_id', $ids)->get();

        foreach ($payments as $key => $pay) {
          foreach ($dets as $key2 => $det) {
            if ($pay->id == $det->invoice_id) {
              $pay->details[] = $det;
            }
          }
        }

        return $payments;
    }

    public function paymentPg(Request $request)
    {
        // $this->validate($request, [
        //     'user_id' => ['required'],
        // ]);
        $payments = DB::table('payments as a')
                ->select(
                    'a.id',
                    'a.payment_no',
                    'a.user_id',
                    'c.first_name',
                    'c.mid_name',
                    'c.last_name',
                    DB::raw('CONCAT_WS(" ", c.first_name, c.last_name) AS user_name'),
                    'a.total_amt',
                    'b.year_name',
                    'a.from_dt',
                    'a.to_dt',
                    'a.status',
                    'classes.id AS class_id',
                    'classes.class_name',
                    'classes.class_sub_name',
                    DB::raw('(CASE
                        WHEN a.pay_method = "02" THEN "Transfer Bank / Virtual Account"
                        WHEN a.pay_method = "03" THEN "Indomaret / Alfamart"
                        WHEN a.pay_method = "03" THEN "E-Wallet / Uang Elektronik (OVO)"
                        ELSE "Other"
                        END) AS pay_method'),
                    'a.txid',
                    'a.vacct_no',
                    'a.bank_cd',
                    'a.created_at'
                )
                ->leftJoin('years as b', 'a.thn_ajaran', '=', 'b.id')
                ->leftJoin('users as c', 'a.user_id', '=', 'c.id')
                ->where(['a.user_id'=> $request->user_id, 'c.type' => 'S'])
                ->leftJoin('user_companies AS usercompany', 'usercompany.user_id', '=', 'c.id')
                ->leftJoin('user_classes AS userclass', 'userclass.user_id', '=', 'a.user_id')
                ->leftJoin('classes AS classes', 'userclass.class_id', '=', 'classes.id')
                // ->where(['a.user_id'=> 24, 'c.type' => 'S'])
                ->orderBy('a.from_dt', 'desc');

        if ($request->has('user_company_id')) {
            if ($request->user_company_id != 2) {
                $payments = $payments->where('usercompany.company_id', $request->user_company_id);
            }
        }

        if ($request->has('class_id')) {
            $payments = $payments->where('classes.id', $request->class_id);
        }

        if ($request->has('user_id')) {
            $payments = $payments->where('a.user_id', $request->user_id);
        }

        $payments = $payments->get();

        $ids = [];
        foreach ($payments as $key => $pay) {
          array_push($ids, $pay->id);
        }

        $dets = InvoicesDet::select(
          'invoices_dets.id',
          'invoices_dets.invoice_id',
          'invoices_dets.prod_id',
          'invoices_dets.prod_nm',
          'invoices_dets.sale_prc',
          'p.type'
                )
                ->join('products AS p', 'p.id', '=', 'invoices_dets.prod_id')
                ->whereIn('invoice_id', $ids)->get();

        foreach ($payments as $key => $pay) {
          foreach ($dets as $key2 => $det) {
            if ($pay->id == $det->invoice_id) {
              $pay->details[] = $det;
            }
          }
        }

        $inv = $this->index($request);

        $paymentDatas = [];
        $cekDate = [];
        $i = 0;
        foreach ($inv as $key => $value) {
            $paymentDatas[$i] = $value;
            $paymentDatas[$i]->status = 'success';
            $cekDate[$i] = $value->from_dt;
            $i++;
        }

        // print_r($cekDate);exit;

        foreach ($payments as $key2 => $value2) {
            $cek = in_array($value2->from_dt, $cekDate);
            if ($cek == false) {
                $paymentDatas[$i] = $value2;
            }
            $i++;
        }

        usort($paymentDatas, array($this,'sortFunction'));

        return $paymentDatas;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            // 'invoice_no' => ['required'],
            // 'user_id' => ['required', 'integer'],
            'total_amt' => ['required', 'between:0,99.99'],
            // 'thn_ajaran' => ['required', 'integer'],
            // 'from_dt' => ['required', 'date'],
            // 'to_dt' => ['required', 'date']
        ]);

        if ($request->month && is_array($request->month)) {
            DB::beginTransaction();

            try {
                $params = [
                    'bankCd' => $request->bank_cd,
                    'mitraCd' => $request->mitra_cd,
                    'amt' => $request->total_amt,
                    'payMethod' => $request->payment_method,
                    'userId' => $request->user_id
                ];

                $responPG = $this->reqPayment($params);

                if (!$responPG) {
                    return ['result' => 'Error in PG', 'code' => '01'];
                }

                $objDemo = new \stdClass();
                $objDemo->resultMsg = $responPG->resultMsg;
                $objDemo->tXid = $responPG->tXid;
                $objDemo->referenceNo = $responPG->referenceNo;
                $objDemo->amt = $responPG->amt;
                $objDemo->bankCd = $this->bankCdToName($responPG->bankCd);
                $objDemo->vacctNo = $responPG->vacctNo;
                $objDemo->billingNm = $responPG->billingNm;
                $objDemo->vacctValidDt = $responPG->vacctValidDt;
                $objDemo->vacctValidTm = $responPG->vacctValidTm;
                $objDemo->receiver = $responPG->billingNm;
                $objDemo->payWay = $this->bankCdToPayWay($responPG->bankCd);
                $objDemo->sender = 'MSIS APP, modern.sis01@gmail.com';
                $responPG->payWay = $this->bankCdToPayWay($responPG->bankCd);

                $userEmail="";

                $user = User::find($request->user_id);
                if ($user) {
                    $userEmail= $user->email;
                }
                $emails = ['modern.sis01@gmail.com', $userEmail];
                Mail::to($emails)->send(new VaCreated($objDemo));
                // print_r($objDemo);exit;

                foreach ($request->month as $key => $value) {
                    $year = Year::find($request->year);
                    if ($year) {
                        if (intval($value) > 6) {
                            $year = explode(' / ', $year->year_name)[0];
                        } else {
                            $year = explode(' / ', $year->year_name)[1];
                        }
                    } else {
                        return false;
                    }


                    $insert = [
                        'from_dt' => date('Y-m-d', strtotime('01-'.$value.'-'.$year)),
                        'to_dt' => date('Y-m-t', strtotime('01-'.$value.'-'.$year)),
                        'payment_no' => $this->selMaxInv(),
                        'user_id' => $request->user_id,
                        'thn_ajaran' => $request->year,
                        'total_amt' => $request->total_amt,
                        'insert_by' => $request->user_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        'status' => 'pending',
                        'pay_method' => $request->payment_method,
                        'txid' => $responPG->tXid,
                        'vacct_no' => $responPG->vacctNo,
                        'bank_cd' => $request->bank_cd,
                        'company_id' => $request->company_id,
                    ];

                    $queryPrep = Payment::where(['from_dt' => date('Y-m-d', strtotime('01-'.$value.'-'.$year)),
                                                'to_dt' => date('Y-m-t', strtotime('01-'.$value.'-'.$year)),
                                                'user_id' => $request->user_id
                                                ]);

                    $invCheck = $queryPrep->count();

                    if ($invCheck <= 0) {
                        $invInput = DB::table('payments')->insert($insert);
                        $invSel = $queryPrep->first();
                        if ($invSel) {
                            if ($request->prod_id && is_array($request->prod_id)) {
                                $invDetRows = [];
                                foreach ($request->prod_id as $key2 => $value2) {
                                    $invDetRows[] = [
                                        'payment_id' => $invSel->id,
                                        'prod_id' => $value2,
                                        'prod_nm' => $request->prod_nm[$key2],
                                        'sale_prc' => $request->sale_prc[$key2],
                                        'insert_by' => $request->user_id,
                                        'created_at' => date('Y-m-d H:i:s'),
                                        'updated_at' => date('Y-m-d H:i:s')
                                    ];
                                }
                                $invInputDets = DB::table('payment_dets')->insert($invDetRows);
                            }
                        }
                    } else {
                        return ['result' => 'Error', 'code' => '01'];
                    }
                }

                DB::commit();
                return ['result' => 'Success', 'code' => '00', 'responsePG' => $responPG];
                // all good
            } catch (\Exception $e) {
                DB::rollback();
                print_r($e->getMessage());
                die;
                return ['result' => $e, 'code' => '01'];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function selMaxInv()
    {
        $inv = 0;
        $maxInv = Payment::where(DB::RAW('SUBSTR(payment_no, 5,6)'), date('ymd'))
                ->max('payment_no');
        // ->toSql();
        // dd($maxInv);
        if (!$maxInv) {
            $inv = 'PAY_'.date('ymd').str_pad("1", 4, "0", STR_PAD_LEFT);
        } else {
            $inv = substr($maxInv, 4);
            $inv = $inv+1;
            $inv = 'PAY_'.$inv;
        }
        return $inv;
    }

    public function reqPayment($params)
    {
        $nicepay = new NicepayLib();

        $dateNow        = date('Ymd');
        $vaExpiryDate   = date('Ymd', strtotime($dateNow . ' +1 day')); // Set VA expiry date +1 day (optional)
        $bankCd         = $params['bankCd'];
        $mitraCd        = $params['mitraCd'];
        $timeStamp      = date('Ymdhis');
        $userId         = $params['userId'];

        $user = User::find($userId);
        // Populate Mandatory parameters to send
        $nicepay->set('timeStamp', $timeStamp); //
        $nicepay->set('referenceNo', $this->selMaxInv()); //
        $nicepay->set('amt', $params['amt']); //
        $nicepay->set('payMethod', $params['payMethod']); //
        $nicepay->set('billingNm', $user->first_name.' '.$user->last_name); //
        $nicepay->set('billingPhone', isset($user->phone_number) ? $user->phone_number : '081219836581'); //
        $nicepay->set('billingEmail', $user->email); //
        $nicepay->set('billingAddr', 'Jl. Jend. Sudirman No. 28');
        $nicepay->set('billingCity', 'Jakarta Pusat'); //
        $nicepay->set('billingState', 'DKI Jakarta'); //
        $nicepay->set('billingPostCd', '10210'); //
        $nicepay->set('billingCountry', 'Indonesia'); //

        // $nicepay->set('merFixAcctId', ''); // For Fix Account
        $nicepay->set('currency', 'IDR'); //
        $nicepay->set('description', 'Payment of : '.$this->selMaxInv()); //
        $nicepay->set('vacctValidDt', $vaExpiryDate); //
        $nicepay->set('vacctValidTm', date('His')); //
        ($params['payMethod'] == '02') ? $nicepay->set('bankCd', $bankCd) : $nicepay->set('mitraCd', $mitraCd); //

        // Send Data
        ($params['payMethod'] == '02') ? $response = $nicepay->requestVA() : $nicepay->set('mitraCd', $mitraCd);
        // print_r($response);exit;
        // Response from NICEPAY
        if (isset($response->resultCd)) {
            return $response;
        }

        return false;
    }

    public function noti(Request $request)
    {
        Log::info('[PaymentController : PaymentController function noti start]');
        $res = '';
        $nicepay = new NicepayLibRedirect();
        //Listen for parameters passed

        $pushParameters = array(
            'tXid',
            'referenceNo',
            'merchantToken',
            'amt',
        );

        Log::info('[PaymentController : PaymentController function noti nicepayLib extractNotification prepare]');
        $nicepay->extractNotification($pushParameters);

        $iMid         = $nicepay->iMid;
        $tXid         = $nicepay->getNotification('tXid');
        $referenceNo  = $nicepay->getNotification('referenceNo');
        $amt          = $nicepay->getNotification('amt');
        $pushedToken  = $nicepay->getNotification('merchantToken');

        $nicepay->set('tXid', $tXid);
        $nicepay->set('amt', $amt);
        $nicepay->set('iMid', $iMid);

        Log::info('[PaymentController : PaymentController function noti prepare get referenceNo]');
        if (isset($referenceNo)) {
            Log::info('[PaymentController : PaymentController function noti referenceNo is isset]');
            $nicepay->set('referenceNo', $referenceNo);
            $transactionToken = $nicepay->merchantTokenC();
            $nicepay->set('merchantToken', $transactionToken);

            Log::info('[PaymentController : PaymentController function noti nicepayLib checkPaymentStatus prepare]');
            $paymentStatus = $nicepay->checkPaymentStatus($tXid, $referenceNo, $amt);
            // print_r($paymentStatus);exit;
            $response = array(
                    'reqTm'         => $paymentStatus->reqTm,
                    // 'instmntType'   => $paymentStatus->instmntType,
                    'resultMsg'     => $paymentStatus->resultMsg,
                    'reqDt'         => $paymentStatus->reqDt,
                    // 'instmntMon'    => $paymentStatus->instmntMon,
                    'status'        => $paymentStatus->status,
                    'tXid'          => $paymentStatus->tXid
            );

            Log::info('[PaymentController : PaymentController function noti cek pushedToken prepare]');
            Log::info('[PaymentController : '.$pushedToken .'=='. $transactionToken.']');
            if ($pushedToken == $transactionToken) {
                Log::info('[PaymentController : PaymentController function noti cek pushedToken verify]');
                if (isset($paymentStatus->status) && $paymentStatus->status == '0') {
                    $res = "Paid";
                    Log::info('[PaymentController : PaymentController function noti status paid]');

                    DB::beginTransaction();
                    Log::info('[PaymentController : PaymentController function noti beginTransaction]');

                    try {
                        // $payment = Payment::where('txid', $paymentStatus->tXid)->first();
                        $payments = Payment::where('txid', $paymentStatus->tXid)->get();
                        foreach ($payments as $key => $value) {
                            $payment = $value;
                            Log::info('[PaymentController : PaymentController function noti cek payment : '.$payment.']');
                            if ($payment) {
                                $payment->status = "success";
                                if ($payment->save()) {
                                    Log::info('[PaymentController : PaymentController function noti cek payment saveed]');
                                    $queryPrep = Invoices::where(['from_dt' => $payment->from_dt,
                                                            'to_dt' => $payment->to_dt,
                                                            'user_id' => $payment->user_id
                                                            ]);
                                    $invCheck = $queryPrep->count();

                                    Log::info('[PaymentController : PaymentController function noti cek invoice exist]');
                                    if ($invCheck <= 0) {
                                        $insert = [
                                                'from_dt' => $payment->from_dt,
                                                'to_dt' => $payment->to_dt,
                                                'invoice_no' => $payment->payment_no,
                                                'user_id' => $payment->user_id,
                                                'thn_ajaran' => $payment->thn_ajaran,
                                                'total_amt' => $payment->total_amt,
                                                'insert_by' => $payment->user_id,
                                                'created_at' => date('Y-m-d H:i:s'),
                                                'updated_at' => date('Y-m-d H:i:s'),
                                            ];
                                        $invInput = DB::table('invoices')->insert($insert);
                                        Log::info('[PaymentController : PaymentController function noti input to invoices table]');

                                        $invSel = $queryPrep->first();
                                        if ($invSel) {
                                            $payDets = PaymentDet::where('payment_id', $payment->id)->get();
                                            Log::info('[PaymentController : PaymentController function noti get paymentDets]');
                                            $invDetRows = [];
                                            foreach ($payDets as $key2 => $value2) {
                                                $invDetRows[] = [
                                                    'invoice_id' => $invSel->id,
                                                    'prod_id' => $value2->prod_id,
                                                    'prod_nm' => $value2->prod_nm,
                                                    'sale_prc' => $value2->sale_prc,
                                                    'insert_by' => $payment->user_id,
                                                    'created_at' => date('Y-m-d H:i:s'),
                                                    'updated_at' => date('Y-m-d H:i:s')
                                                ];
                                            }
                                            Log::info('[PaymentController : PaymentController function noti input paymentDets]');
                                            $invInputDets = DB::table('invoices_dets')->insert($invDetRows);
                                        }
                                    }
                                }
                            }
                        }
                        $objDemo = new \stdClass();
                        $objDemo->tXid = $paymentStatus->tXid;
                        $objDemo->referenceNo = $paymentStatus->referenceNo;
                        $objDemo->amt = $paymentStatus->amt;
                        $objDemo->bankCd = $paymentStatus->bankCd;
                        $objDemo->vacctNo = $paymentStatus->vacctNo;
                        $objDemo->billingNm = $paymentStatus->billingNm;
                        $objDemo->receiver = $paymentStatus->billingNm;
                        $objDemo->sender = 'MSIS APP, modern.sis01@gmail.com';

                        Log::info('[PaymentController : PaymentController function noti send email]');

                        $userEmail="";

                        $user = User::find($payments[0]->user_id);
                        if ($user) {
                            $userEmail= $user->email;
                        }

                        $emails = ['modern.sis01@gmail.com', $userEmail];
                        Mail::to($emails)->send(new VaSuccess($objDemo));

                        Log::info('[PaymentController : PaymentController function noti commit transaction]');
                        DB::commit();
                        return ['result' => $res, 'code' => '00'];
                    } catch (\Exception $e) {
                        Log::info('[PaymentController : PaymentController function noti rollback transaction]');
                        DB::rollback();
                        return ['result' => $e, 'code' => '01'];
                    }
                } elseif (isset($paymentStatus->status) && $paymentStatus->status == '1') {
                    $res = "<pre>Reversal</pre>";
                } elseif (isset($paymentStatus->status) && $paymentStatus->status == '3') {
                    $res = "<pre>Cancel</pre>";
                } elseif (isset($paymentStatus->status) && $paymentStatus->status == '4') {
                    $res = "<pre>Expired</pre>";
                } else {
                    $res = "<pre>Status Unknown</pre>";
                }
            }
        }
        return $res;
    }

    private function sortFunction($a, $b)
    {
        if (strtotime($a->created_at) < strtotime($b->created_at)) {
            return 1;
        } elseif (strtotime($a->created_at) > strtotime($b->created_at)) {
            return -1;
        } else {
            return 0;
        }
        // return strtotime($a->from_dt) - strtotime($b->from_dt);
    }

    private function bankCdToName($bankCd)
    {
        $bankName = '';
        $bank = Bank::where('bank_code', $bankCd)->first();
        if ($bank) {
            $bankName = $bank->bank_code;
        }

        return $bankName;
    }

    private function bankCdToPayWay($bankCd)
    {
        $content = '';
        $payWay = PayWay::where('bank_code', $bankCd)->first();
        if ($payWay) {
            $content = $payWay->description;
        }


        return $content;
    }

}
