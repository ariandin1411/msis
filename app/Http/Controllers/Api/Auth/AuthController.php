<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Passport\Client;
use Illuminate\Support\Facades\Route;
use App\User;
use App\Models\UserGuardian;
use Auth;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{
    private $client;

    public function __construct()
    {
        $this->client = Client::find(2);
        $this->middleware('auth:api', ['except' => ['index','resetPassword']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
          'email' => 'required|email',
          'password' => 'required'
        ]);

        $params = [
          'grant_type' => 'password',
          'client_id' => $this->client->id,
          'client_secret' => $this->client->secret,
          'username' => request('email'),
          'password' => request('password'),
          'scope' => '*'
        ];

        try {
            $user = User::where('email', request('email'))->first();
            if ($user->type == 'G') {
                $userGuardians = UserGuardian::where('guardian_user_id', $user->id)->get();
                $students = [];
                foreach ($userGuardians as $key => $value) {
                    $student = User::Find($value->student_user_id);
                    if ($student) {
                        $students[$key] = $student;
                    }
                }
                // echo $students; die;
                $user['students'] = $students;
            }

            $user->company = $user->company;
            $user->class = $user->class;
            $user->role = $user->role;
            $request->request->add($params);
            $proxy = Request::create('oauth/token', 'POST');
            $token = Route::dispatch($proxy);
            $tokenContent = json_decode($token->getContent(), true);
            if (!isset($tokenContent['error'])) {
              $tokenContent["user"] = $user;
            }
        } catch (\Exception $e) {
            print_r($e->getMessage());
            die;
        }


        return $tokenContent;
    }

    public function refresh(Request $request)
    {
        $this->validate($request, [
        'email' => 'required|email',
        'password' => 'required',
        'refresh_token' => 'required',
      ]);

        $params = [
        'grant_type' => 'refresh_token',
        'client_id' => $this->client->id,
        'client_secret' => $this->client->secret,
        'username' => request('email'),
        'password' => request('password'),
        'scope' => '*'
      ];

        $request->request->add($params);

        $proxy = Request::create('oauth/token', 'POST');

        return Route::dispatch($proxy);
    }

    public function logout(Request $request)
    {
        $accessToken = Auth::user()->token();

        DB::table('oauth_refresh_tokens')
      ->where('access_token_id', $accessToken->id)
      ->update(['revoked' => true]);

        $accessToken->revoke();

        return response()->json([], 204);
    }

    public function resetPassword(Request $request)
    {
        $request->validate(['email' => 'required|email']);

        $credentials = ['email' => $request->email];
        $response = Password::sendResetLink($credentials, function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return "success";
            case Password::INVALID_USER:
                return "failed";
        }
    }
}
