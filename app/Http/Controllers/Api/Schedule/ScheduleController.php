<?php

namespace App\Http\Controllers\Api\Schedule;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Schedule;

class ScheduleController extends Controller
{
    public function __construct(){
      $this->middleware('auth:api', ['except' => ['']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $schedule = Schedule::where('active', 1);

        if( $request->has('user_company_id') ){
            if( $request->user_company_id != 2 ) {
                $schedule = $schedule->where('company_id', $request->user_company_id);
            }
        }

        if( $request->has('lesson_hour_id') ){
            $schedule = $schedule->where('lesson_hour_id', $request->lesson_hour_id);
        }

        if( $request->has('day_name') ){
            $schedule = $schedule->where('day_name', $request->day_name);
        }

        if( $request->has('semester') ){
            $schedule = $schedule->where('semester', $request->semester);
        }

        if( $request->has('year_id') ){
            $schedule = $schedule->where('year_id', $request->year_id);
        }

        if( $request->has('class_id') ){
            $schedule = $schedule->where('class_id', $request->class_id);
        }

        $schedule = $schedule->get();

        foreach ($schedule as $key => $value) {
            $value->company = $value->company;
            $value->user = $value->user;
            $value->lesson_hour = $value->lesson_hour;
            $value->classes = $value->classes;
            $value->subject = $value->subject;
        }

        return response()->json($schedule, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subject_id' => ['required', 'integer'],
            'user_id' => ['required', 'integer'],
            'lesson_hour_id' => ['required', 'integer'],
            'day_name' => ['required', 'string'],
            'schedule_type' => ['required', 'string'],
            'year_id' => ['required', 'integer'],
            'semester' => ['required', 'integer'],
            'class_id' => ['required', 'integer'],
            'company_id' => ['required', 'integer'],
        ]);

        $scheduleCheck = Schedule::where(
          [
//          'user_id' => $request->user_id,
//          'subject_id' => $request->subject_id,
            'lesson_hour_id' => $request->lesson_hour_id,
            'day_name' => $request->day_name,
            'year_id' => $request->year_id,
            'semester' => $request->semester,
            'class_id' => $request->class_id,
            'company_id' => $request->company_id
          ]
        )->count();

        if($scheduleCheck > 0){
          $schedule = Schedule::where('active', 1)->latest()->first();
          return response()->json($schedule, 200);
        }

        $input = $request->all();
        $input['active'] = 1;
        $input['insert_by'] = $request->insert_user_id;
        $schedule = Schedule::create($input);
        if($schedule){
            return response()->json($schedule, 200);
        }

        $message = ['msg' => 'Error when input data', 'code' => '0500'];

        return response()->json($message, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $schedule = Schedule::find($id);

        if($schedule){
            return response()->json($schedule, 200);
        }

        $message = ['msg' => 'Data not found', 'code' => '0404'];

        return response()->json($message, 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'subject_id' => ['required', 'integer'],
            'user_id' => ['required', 'integer'],
            'lesson_hour_id' => ['required', 'integer'],
            'day_name' => ['required', 'string'],
            'schedule_type' => ['required', 'string'],
            'year_id' => ['required', 'integer'],
            'semester' => ['required', 'integer'],
            'class_id' => ['required', 'integer'],
            'company_id' => ['required', 'integer'],
        ]);

        $schedule = Schedule::find($id);
        $input = $request->all();
        $input['update_by'] = $request->user_id;
        if($schedule){
            $scheduleUpdate = $schedule->update($input);
            if($scheduleUpdate){
                $schedule = Schedule::find($id);
                return response()->json($schedule, 200);
            }
        }

        $message = ['msg' => 'Error when update data', 'code' => '0500'];
        return response()->json($message, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedule = Schedule::find($id);
        if($schedule){
            $schedule->update(['active' => 0]);
            if($schedule){
                $message = ['msg' => 'Delete data success', 'code' => '0000'];
                return response()->json($message, 200);
            }
        }

        $message = ['msg' => 'Error when delete data', 'code' => '0500'];
        return response()->json($message, 500);
    }


    public function getDayByCompanyClasses(Request $request)
    {
        if(!$request->has('company_id')){
            $message = ['msg' => 'Company id must filles', 'code' => '0500'];
            return response()->json($message, 404);
        }

        if(!$request->has('class_id')){
            $message = ['msg' => 'Class id must filles', 'code' => '0500'];
            return response()->json($message, 404);
        }

        if(!$request->has('day_name')){
            $message = ['msg' => 'day name must filles', 'code' => '0500'];
            return response()->json($message, 404);
        }

      if(!$request->has('semester')){
        $message = ['msg' => 'semester name must filles', 'code' => '0500'];
        return response()->json($message, 404);
      }

      if(!$request->has('year_id')){
        $message = ['msg' => 'year id must filles', 'code' => '0500'];
        return response()->json($message, 404);
      }

        $schedule = Schedule::where('company_id', $request->company_id)
                    ->where('class_id', $request->class_id)
                    ->where('day_name', $request->day_name)
                    ->where('class_id', $request->semester)
                    ->where('class_id', $request->year_id)
                    ->get();

        if($schedule){
            return response()->json($schedule, 200);
        }

        $message = ['msg' => 'Data not found', 'code' => '0404'];

        return response()->json($message, 404);
    }
}
