<?php

namespace App\Http\Controllers\Api\Installment;

use App\User;
use App\Models\Installment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class InstallmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $installment = [];
        if ($request->class_id && $request->company_id) {
            $installment = User::select(
                'users.first_name'
                ,'users.mid_name'
                ,'users.last_name'
                ,'com.company_name'
                ,'users.nis'
                ,'c.class_name'
                ,'c.class_sub_name'
                ,'c.major'
                ,'p.product_name'
                // ,DB::RAW("sum(i.installment_amount) AS installment_amount")
                ,'i.installment_amount'
                ,'i.loan_amount'
                ,'i.rest_of_loan'
                ,'users.id'
                ,'p.id AS prod_id'
            )
            ->leftJoin('installments AS i', function ($join) use ($request) {
              $join->on('users.id', '=', 'i.user_id');
//                ->leftJoin('installments AS i2', function ($join) {
//                  $join->on('i.id', '=', 'i2.id')
//                  ->on('i.id', '<', 'i2.id')
//                    ->whereNull('i2.id');
//                });
            })
            // ->leftJoin('installments AS i', 'users.id', '=', 'i.user_id')
            // ->leftJoin('installments AS i2', 'users.id', '=', 'i.user_id')
            ->leftJoin('products AS p', 'i.prod_id', '=', 'p.id')
            ->leftJoin('user_classes AS uc', 'uc.user_id', '=', 'users.id')
            ->leftJoin('classes AS c', 'c.id', '=', 'uc.class_id')
            ->leftJoin('user_companies AS ucs', 'ucs.user_id', '=', 'users.id')
            ->leftJoin('companies AS com', 'com.id', '=', 'ucs.company_id')
            ->where('users.active', 1)
            ->where('c.id', $request->class_id)
            ->where('ucs.company_id', $request->company_id)
            // ->groupBy('i.prod_id')
            ->orderBy('users.first_name');

            $installment = $installment->get();
        }

        return $installment;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showByUserIdandProdId(Request $request)
    {
      $this->validate($request, [
        'user_id' => ['required'],
        // 'prod_id' => ['required'],
      ]);

      $installment = Installment::query()
                      ->where('user_id', $request->user_id);

      if ($request->has('prod_id')) {
        $installment = $installment->where('prod_id', $request->prod_id);
      }

      $installment = $installment->orderBy('created_at', 'desc')->get();

      return response()->json($installment, 201);
    }
}
