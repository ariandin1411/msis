<?php

namespace App\Http\Controllers\Api\Subject;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subject;

class SubjectController extends Controller
{
    public function __construct(){
      $this->middleware('auth:api', ['except' => ['']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $subject = Subject::where('active', 1);

        if( $request->has('user_company_id') ){
            if( $request->user_company_id != 2 ) {
                $subject = $subject->where('company_id', $request->user_company_id);
            }
        }

        $subject = $subject->get();

        foreach ($subject as $key => $value) {
            $value->company = $value->company;
        }

        return response()->json($subject, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subject_name' => ['required', 'string', 'max:191'],
            'company_id' => ['required'],
        ]);

        $input = $request->all();
        $input['active'] = 1;
        $input['insert_by'] = $request->user_id;
        $subject = Subject::create($input);
        if($subject){
            return response()->json($subject, 200);
        }

        $message = ['msg' => 'Error when input data', 'code' => '0500'];

        return response()->json($message, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subject = Subject::find($id);

        if($subject){
            return response()->json($subject, 200);
        }

        $message = ['msg' => 'Data not found', 'code' => '0404'];

        return response()->json($message, 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'subject_name' => ['required', 'string', 'max:191'],
            'company_id' => ['required'],
        ]);

        $subject = Subject::find($id);
        $input = $request->all();
        $input['update_by'] = $request->user_id;
        if($subject){
            $subjectUpdate = $subject->update($input);
            if($subjectUpdate){
                $subject = Subject::find($id);
                return response()->json($subject, 200);
            }
        }

        $message = ['msg' => 'Error when update data', 'code' => '0500'];
        return response()->json($message, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subject = Subject::find($id);
        if($subject){
            $subject->update(['active' => 0]);
            if($subject){
                $message = ['msg' => 'Delete data success', 'code' => '0000'];
                return response()->json($message, 200);
            }
        }

        $message = ['msg' => 'Error when delete data', 'code' => '0500'];
        return response()->json($message, 500);
    }
}
