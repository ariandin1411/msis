<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    public function __construct(){
      $this->middleware('auth:api', ['except' => ['']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $product = Product::where('active', 1);

        if( $request->has('user_company_id') ){
            if( $request->user_company_id != 2 ) {
                $product = $product->where('company_id', $request->user_company_id);
            }
        }

        if( $request->has('class_id') ){
                $product = $product->where('class_id', $request->class_id);
        }

        $product = $product->get();

        foreach ($product as $key => $value) {
            $value->company = $value->company;
            $value->class = $value->class;
        }

        return response()->json($product, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_name' => ['required', 'string', 'max:191'],
            'company_id' => ['required'],
            'sale_price' => ['required', 'integer'],
        ]);

        $input = $request->all();
        $input['active'] = 1;
        $input['insert_by'] = $request->user_id;
        $product = Product::create($input);
        if($product){
            return response()->json($product, 200);
        }

        $message = ['msg' => 'Error when input data', 'code' => '0500'];

        return response()->json($message, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        if($product){
            $product->company = $product->company;
            return response()->json($product, 200);
        }

        $message = ['msg' => 'Data not found', 'code' => '0404'];

        return response()->json($message, 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_name' => ['required', 'string', 'max:191'],
            'company_id' => ['required'],
            'sale_price' => ['required', 'integer'],
        ]);

        $product = Product::find($id);
        $input = $request->all();
        $input['update_by'] = $request->user_id;
        if($product){
            $productUpdate = $product->update($input);
            if($productUpdate){
                $product = Product::find($id);
                return response()->json($product, 200);
            }
        }

        $message = ['msg' => 'Error when update data', 'code' => '0500'];
        return response()->json($message, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if($product){
            $product->update(['active' => 0]);
            if($product){
                $message = ['msg' => 'Delete data success', 'code' => '0000'];
                return response()->json($message, 200);
            }
        }

        $message = ['msg' => 'Error when delete data', 'code' => '0500'];
        return response()->json($message, 500);
    }
}
