<?php

namespace App\Http\Controllers\Api\Note;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Note;

class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $note = Note::where('active', 1);

        if( $request->has('user_company_id') ){
            if( $request->user_company_id != 2 ) {
                $note = $note->where('company_id', $request->user_company_id);
            }
        }

        $note = $note->orderBy('id', 'DESC')->get();

        foreach ($note as $key => $value) {
            $value->company = $value->company;
        }

        return response()->json($note, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => ['required'],
            'message' => ['required'],
            'company_id' => ['required'],
        ]);

        $input = $request->all();
        $input['active'] = 1;
        $input['insert_by'] = $request->user_id;
        $note = Note::create($input);
        if($note){
            return response()->json($note, 200);
        }

        $message = ['msg' => 'Error when input data', 'code' => '0500'];

        return response()->json($message, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $note = Product::find($id);

        if($note){
            $note->company = $note->company;
            return response()->json($note, 200);
        }

        $message = ['msg' => 'Data not found', 'code' => '0404'];

        return response()->json($message, 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => ['required'],
            'message' => ['required'],
            'company_id' => ['required'],
        ]);

        $note = Note::find($id);
        $input = $request->all();
        $input['update_by'] = $request->user_id;
        if($note){
            $noteUpdate = $note->update($input);
            if($noteUpdate){
                $note = Product::find($id);
                return response()->json($note, 200);
            }
        }

        $message = ['msg' => 'Error when update data', 'code' => '0500'];
        return response()->json($message, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note = Note::find($id);
        if($note){
            $note->update(['active' => 0]);
            if($note){
                $message = ['msg' => 'Delete data success', 'code' => '0000'];
                return response()->json($message, 200);
            }
        }

        $message = ['msg' => 'Error when delete data', 'code' => '0500'];
        return response()->json($message, 500);
    }
}
