<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\UserRole;
use App\Models\StudentGuardian;
use App\Models\UserCompany;
use App\Models\Classes;
use App\Models\UserClass;
use App\Models\UserGuardian;
use DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $users = User::where('active', 1);

        if ($request->has('type')) {
            $users = $users->where('type', $request->type);
        }

        $users = $users->get();
        $usersArr = [];
        $i = 0;

        foreach ($users as $key => $value) {
            if ($request->has('user_company_id') && $request->has('class_id') && $value->class) {
                if ($value->company->id == $request->user_company_id && $value->class->id == $request->class_id) {
                    $value->role = $value->role;
                    $value->student_guardian = $value->student_guardian;
                    $value->guardian_of = $value->user_guardian;
                    $value->class = $value->class;
                    $value->company = $value->company;
                    $usersArr[$i] = $value;
                    $i++;
                }
            } elseif ($request->has('user_company_id')) {
                if ($value->company->id == $request->user_company_id) {
                    $value->role = $value->role;
                    $value->student_guardian = $value->student_guardian;
                    $value->guardian_of = $value->user_guardian;
                    $value->class = $value->class;
                    $value->company = $value->company;
                    $usersArr[$i] = $value;
                    $i++;
                }
            } elseif ($request->has('class_id') && $value->class) {
                if ($value->class->id == $request->class_id) {
                    $value->role = $value->role;
                    $value->student_guardian = $value->student_guardian;
                    $value->guardian_of = $value->user_guardian;
                    $value->class = $value->class;
                    $value->company = $value->company;
                    $usersArr[$i] = $value;
                    $i++;
                }
            } else {
                $value->role = $value->role;
                $value->student_guardian = $value->student_guardian;
                $value->guardian_of = $value->user_guardian;
                $value->class = $value->class;
                $value->company = $value->company;
                $usersArr[$i] = $value;
                $i++;
            }
        }

        return response()->json($usersArr, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:191'],
            'last_name' => ['nullable', 'string', 'max:191'],
            'email' => ['required', 'string', 'email', 'max:191', 'unique:users'],
            'nig' => ['nullable', 'string', 'max:191'],
            'nis' => ['nullable', 'string', 'max:191'],
            'gender' => ['required', 'string', 'max:191'],
            'phone_number' => ['required', 'string', 'max:191'],
            'year_of_entry' => ['required', 'date', 'max:191'],
            'birth_date' => ['required', 'date', 'max:191'],
            'born_at' => ['required', 'string', 'max:191'],
            'religion' => ['required', 'string', 'max:191'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        DB::beginTransaction();

        try {
            $input = $request->all();
            $input['active'] = 1;
            $input['insert_by'] = $request->user_id;
            $input['password'] =  Hash::make($request->password);
            $user = User::create($input);
            if ($user) {

            //Insert User Company
                $userCompany = new UserCompany();
                $userCompany->user_id = $user->id;
                $userCompany->company_id = $request->company_id;
                $userCompany->active = 1;
                $userCompany->insert_by = $user->id;
                $userCompany->save();

                //insert User role
                if ($request->type == 'A') {
                    $userRole = new UserRole();
                    $userRole->user_id = $user->id;
                    $userRole->role_id = 1; // this role for Admin
                    $userRole->active = 1;
                    $userRole->insert_by = $user->id;
                    $userRole->save();
                } elseif ($request->type == 'G') {
                    $userRole = new UserRole();
                    $userRole->user_id = $user->id;
                    $userRole->role_id = 2; // this role for guardian
                    $userRole->active = 1;
                    $userRole->insert_by = $user->id;
                    $userRole->save();

                    if ($request->student_guardians) {

                        //Insert User Guardian
                        foreach ($request->student_guardians as $key => $value) {
                            $userGuardian = new UserGuardian();
                            $userGuardian->student_user_id = $value;
                            $userGuardian->guardian_user_id = $user->id;
                            $userGuardian->company_id = $request->company_id;
                            $userGuardian->active = 1;
                            $userGuardian->insert_by = $user->id;
                            $userGuardian->save();
                        }
                    }
                } elseif ($request->type == 'T') {
                    $userRole = new UserRole();
                    $userRole->user_id = $user->id;
                    $userRole->role_id = 3; // this role for teacher
                    $userRole->active = 1;
                    $userRole->insert_by = $user->id;
                    $userRole->save();
                } elseif ($request->type == 'TT') {
                  $userRole = new UserRole();
                  $userRole->user_id = $user->id;
                  $userRole->role_id = 5; // this role for teacher
                  $userRole->active = 1;
                  $userRole->insert_by = $user->id;
                  $userRole->save();
                } elseif ($request->type == 'S') {
                    $userRole = new UserRole();
                    $userRole->user_id = $user->id;
                    $userRole->role_id = 4; // this role for student
                    $userRole->active = 1;
                    $userRole->insert_by = $user->id;
                    $userRole->save();

                    //Insert Student Guardian
                    $studentGuardian = new StudentGuardian();
                    $studentGuardian->user_id = $user->id;
                    $studentGuardian->name = $request->guardian_name;
                    $studentGuardian->phone_number = $request->guardian_phone_number;
                    $studentGuardian->email = $request->guardian_email;
                    $studentGuardian->active = 1;
                    $studentGuardian->insert_by = $user->id;
                    $studentGuardian->save();

                    //Insert User Class
                    $userClass = new UserClass();
                    $userClass->user_id = $user->id;
                    $userClass->class_id = $request->class_id;
                    $userClass->active = 1;
                    $userClass->insert_by = $user->id;
                    $userClass->save();
                }
                DB::commit();


                return response()->json($user, 200);
            }
        } catch (\Exception $e) {
            DB::rollback();
            // print_r($e->getMessage());die;
            // return ['result' => $e, 'code' => '01'];
            $message = ['msg' => $e->getMessage(), 'code' => '0500'];
            return response()->json($message, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if ($user) {
            $user->role = $user->role;
            $user->company = $user->company;
            $user->guardian_of = $user->user_guardian;
            $user->student_guardian = $user->student_guardian;
            $user->class = $user->class;

            return response()->json($user, 200);
        }

        $data = ['data not found'];
        return response()->json($data, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:191'],
            'last_name' => ['nullable', 'string', 'max:191'],
            'nig' => ['nullable', 'string', 'max:191'],
            'nis' => ['nullable', 'string', 'max:191'],
            'gender' => ['required', 'string', 'max:191'],
            'phone_number' => ['required', 'string', 'max:191'],
            'year_of_entry' => ['required', 'date', 'max:191'],
            'birth_date' => ['required', 'date', 'max:191'],
            'born_at' => ['required', 'string', 'max:191'],
            'religion' => ['required', 'string', 'max:191'],
        ]);

        $user = User::find($id);
        $input = $request->all();

        if( $request->has('password') ){
          $input['password'] =  Hash::make($request->password);
        }

        $input['update_by'] = $request->user_id;
        if ($user) {
            DB::beginTransaction();
            try {
                $userUpdate = $user->update($input);

                if ($request->type == 'G') {
                    if ($request->student_guardians) {
                        //delete existing user guardian
                        UserGuardian::where('guardian_user_id',$id)->delete();
                        //Update User Guardian
                        foreach ($request->student_guardians as $key => $value) {

                            $userGuardian = new UserGuardian();
                            $userGuardian->student_user_id = $value;
                            $userGuardian->guardian_user_id = $user->id;
                            $userGuardian->company_id = $request->company_id;
                            $userGuardian->active = 1;
                            $userGuardian->insert_by = $user->id;
                            $userGuardian->save();
                        }
                    }
                }

                if ($request->type == 'A') {
                  $userCompany = UserCompany::where('user_id', $id)->first();
                  if ($userCompany) {
                    $userCompany->company_id = $request->company_id;
                    $userCompany->update_by = $request->user_id;
                    $userCompany->save();
                  }
                }


                if ($userUpdate) {
                    $studentGuardian = StudentGuardian::where('user_id', $id)->first();

                    if ($studentGuardian) {
                        $inputstudentGuardian['name'] = $request->guardian_name;
                        $inputstudentGuardian['phone_number'] = $request->guardian_phone_number;
                        $inputstudentGuardian['email'] = $request->guardian_email;
                        $inputstudentGuardian['update_by'] = $request->user_id;

                        $studentGuardianUpdate = $studentGuardian->update($inputstudentGuardian);

                        if ($studentGuardianUpdate) {
                            //Update User Class
                            $userClass = UserClass::where('user_id', $id)->first();
                            if ($userClass) {
                                $userClass->class_id = $request->class_id;
                                $userClass->update_by = $request->user_id;
                                $userClass->save();
                            } else {
                                //Insert User Class
                                $userClass = new UserClass();
                                $userClass->user_id = $id;
                                $userClass->class_id = $request->class_id;
                                $userClass->active = 1;
                                $userClass->insert_by = $user->id;
                                $userClass->save();
                            }
                            DB::commit();

                            $user = User::find($id);

                            $user->role = $user->role;
                            $user->company = $user->company;
                            $user->student_guardian = $user->student_guardian;
                            $user->class = $user->class;
                            return response()->json($user, 200);
                        }
                    } else {
                        $user = User::find($id);
                        DB::commit();

                        $user->role = $user->role;
                        $user->company = $user->company;
                        $user->student_guardian = $user->student_guardian;
                        $user->class = $user->class;
                        return response()->json($user, 200);
                    }
                }
            } catch (\Exception $e) {
                DB::rollback();
                $message = ['msg' => $e->getMessage(), 'code' => '0500'];
                return response()->json($message, 500);
            }
        }

        $message = ['msg' => 'Error when update data', 'code' => '0500'];
        return response()->json($message, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user) {
            $user->update(['active' => 0]);
            if ($user) {
                $message = ['msg' => 'Delete data success', 'code' => '0000'];
                return response()->json($message, 200);
            }
        }

        $message = ['msg' => 'Error when delete data', 'code' => '0500'];
        return response()->json($message, 500);
    }

    public function changeClasses(Request $request)
    {
        // return $request;
        DB::beginTransaction();

        try {
            $i = 0;
            foreach ($request->userIds as $key => $userId) {
                $count = UserClass::where('user_id', $userId)->count();
                if ($count > 0) {
                    $userClass = UserClass::where('user_id', $userId)->first();
                } else {
                    $userClass = new UserClass();
                    $userClass->user_id = $userId;
                    $userClass->active = 1;
                    $userClass->insert_by = $request->user_id;
                }

                $activeClasses = Classes::find($request->kelas);

                $userClass->active = $activeClasses->active;
                $userClass->class_id = $request->kelas;

                if ($userClass->save()) {
                    $i++;
                } else {
                    $i = 0;
                    break;
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return ['result' => $e, 'code' => '01'];
        }

        if ($i > 0) {
            return ['message' => 'Success', 'result' => '0'];
        }

        return ['message' => 'Error', 'result' => '1'];
    }
}
