<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserGuardian;

class UserGuardianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userGuardians = UserGuardian::where('active', 1);

        if ($request->has('user_company_id')) {
            if ($request->user_company_id != 2) {
                $userGuardians = $userGuardians->where('company_id', $request->user_company_id);
            }
        }

        if ($request->has('student_user_id')) {
            $userGuardians = $userGuardians->where('student_user_id', $request->user_id);
        }

        if ($request->has('guardian_user_id')) {
            $userGuardians = $userGuardians->where('guardian_user_id', $request->user_id);
        }

        $userGuardians = $userGuardians->get();

        foreach ($userGuardians as $key => $value) {
            $value->company = $value->company;
            $value->user_student = $value->user_student;
            $value->user_guardian = $value->user_guardian;
        }


        return response()->json($userGuardians, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
