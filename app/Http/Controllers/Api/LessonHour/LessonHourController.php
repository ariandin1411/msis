<?php

namespace App\Http\Controllers\Api\LessonHour;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LessonHour;

class LessonHourController extends Controller
{
    public function __construct(){
      $this->middleware('auth:api', ['except' => ['']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lessonHour = LessonHour::where('active', 1);

        if( $request->has('user_company_id') ){
            if( $request->user_company_id != 2 ) {
                $lessonHour = $lessonHour->where('company_id', $request->user_company_id);
            }
        }

        $lessonHour = $lessonHour->get();

        foreach ($lessonHour as $key => $value) {
            $value->company = $value->company;
        }

        return response()->json($lessonHour, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'time_name' => ['required', 'string', 'max:191'],
            'from_time' => ['required'],
            'to_time' => ['required'],
            'company_id' => ['required', 'integer'],
        ]);

        $input = $request->all();
        $input['active'] = 1;
        $input['insert_by'] = $request->user_id;
        $lessonHour = LessonHour::create($input);
        if($lessonHour){
            return response()->json($lessonHour, 200);
        }

        $message = ['msg' => 'Error when input data', 'code' => '0500'];

        return response()->json($message, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lessonHour = LessonHour::find($id);

        if($lessonHour){
            return response()->json($lessonHour, 200);
        }

        $message = ['msg' => 'Data not found', 'code' => '0404'];

        return response()->json($message, 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'time_name' => ['required', 'string', 'max:191'],
            'from_time' => ['required'],
            'to_time' => ['required'],
            'company_id' => ['required', 'integer'],
        ]);

        $lessonHour = LessonHour::find($id);
        $input = $request->all();
        $input['update_by'] = $request->user_id;
        if($lessonHour){
            $lessonHourUpdate = $lessonHour->update($input);
            if($lessonHourUpdate){
                $lessonHour = LessonHour::find($id);
                return response()->json($lessonHour, 200);
            }
        }

        $message = ['msg' => 'Error when update data', 'code' => '0500'];
        return response()->json($message, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lessonHour = LessonHour::find($id);
        if($lessonHour){
            $lessonHour->update(['active' => 0]);
            if($lessonHour){
                $message = ['msg' => 'Delete data success', 'code' => '0000'];
                return response()->json($message, 200);
            }
        }

        $message = ['msg' => 'Error when delete data', 'code' => '0500'];
        return response()->json($message, 500);
    }
}
