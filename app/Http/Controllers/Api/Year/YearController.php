<?php

namespace App\Http\Controllers\Api\Year;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Year;

class YearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $year = Year::where('active', 1)->get();
        return response()->json($year, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'year_name' => ['required', 'string', 'max:191'],
        ]);

        $input = $request->all();
        $input['active'] = 1;
        $input['insert_by'] = $request->user_id;
        $year = Year::create($input);
        if($year){
            return response()->json($year, 200);
        }

        $message = ['msg' => 'Error when input data', 'code' => '0500'];

        return response()->json($message, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $year = Year::find($id);

        if($year){
            return response()->json($year, 200);
        }

        $message = ['msg' => 'Data not found', 'code' => '0404'];

        return response()->json($message, 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'year_name' => ['required', 'string', 'max:191'],
        ]);

        $year = Year::find($id);
        $input = $request->all();
        $input['update_by'] = $request->user_id;
        if($year){
            $yearUpdate = $year->update($input);
            if($yearUpdate){
                $year = Year::find($id);
                return response()->json($year, 200);
            }
        }

        $message = ['msg' => 'Error when update data', 'code' => '0500'];
        return response()->json($message, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $year = Year::find($id);
        if($year){
            $year->update(['active' => 0]);
            if($year){
                $message = ['msg' => 'Delete data success', 'code' => '0000'];
                return response()->json($message, 200);
            }
        }

        $message = ['msg' => 'Error when delete data', 'code' => '0500'];
        return response()->json($message, 500);
    }
}
