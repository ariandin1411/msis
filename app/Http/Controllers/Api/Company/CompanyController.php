<?php

namespace App\Http\Controllers\Api\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Province;
use App\Models\City;
use App\Models\District;
use App\Models\Village;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Company::where('active', 1)->get();

        return response()->json($company, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company_name' => ['required', 'string', 'max:191'],
            'email' => ['required', 'email', 'unique:companies,email'],
            'address' => ['required', 'string', 'max:191'],
            'classification' => ['required', 'string', 'max:191'],
        ]);

        $input = $request->all();
        $input['active'] = 1;
        $input['insert_by'] = $request->user_id;
        $company = Company::create($input);
        if ($company) {
            return response()->json($company, 200);
        }

        $message = ['msg' => 'Error when input data', 'code' => '0500'];

        return response()->json($message, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);

        if ($company) {
            return response()->json($company, 200);
        }

        $message = ['msg' => 'Data not found', 'code' => '0404'];

        return response()->json($message, 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'company_name' => ['required', 'string', 'max:191'],
            'email' => ['required','email'],
            'address' => ['required', 'string', 'max:191'],
            'classification' => ['required', 'string', 'max:191'],
        ]);

        $company = Company::find($id);
        $input = $request->all();
        $input['update_by'] = $request->user_id;
        if ($company) {
            $companyUpdate = $company->update($input);
            if ($companyUpdate) {
                $company = Company::find($id);
                return response()->json($company, 200);
            }
        }

        $message = ['msg' => 'Error when update data', 'code' => '0500'];
        return response()->json($message, 500);
    }

    public function province()
    {
        return Province::OrderBy('name')->get();
    }

    public function city(Request $request, $id)
    {
        return City::where('province_id', $id)->OrderBy('name')->get();
    }

    public function district(Request $request, $id)
    {
        return District::where('city_id', $id)->OrderBy('name')->get();
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        if ($company) {
            $company->update(['active' => 0]);
            if ($company) {
                $message = ['msg' => 'Delete data success', 'code' => '0000'];
                return response()->json($message, 200);
            }
        }

        $message = ['msg' => 'Error when delete data', 'code' => '0500'];
        return response()->json($message, 500);
    }
}
