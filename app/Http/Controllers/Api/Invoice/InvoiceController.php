<?php

namespace App\Http\Controllers\Api\Invoice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Invoices;
use App\Models\Installment;
use App\Models\Product;
use App\Models\InvoicesDet;
use App\User;
use App\Models\Year;
use Auth;
use DB;
use Illuminate\Support\Facades\Log;
use App\Exports\InvoicesExport;
use Excel;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['exportExcel']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Log::info('[InvoiceController : Masuk ke InvoiceController function index]');
        // $thnAjaran = '2019 / 2020';
        // $datemonth = substr($thnAjaran, 0, 4);
        // $datemonth2 = substr($thnAjaran, -4);

        $invoiceUsers = [];
        if ($request->year_id && $request->class_id) {
            $yearInfo = Year::find($request->year_id);
            $thnAjaran = $yearInfo->year_name;
            $datemonth = substr($yearInfo->year_name, 0, 4);
            $datemonth2 = substr($yearInfo->year_name, -4);


            $invoiceUsers = User::select(
                'users.id',
                'users.first_name',
                'users.last_name',
                'users.nis',
                'c.class_name',
                'c.class_sub_name',
                'c.major',
                'i.id AS invoice_id',
                'usercompany.company_id',
                DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "' . $datemonth . '-07",concat(idets.sale_prc,"_",i.id),null))) as jul'),
                DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "' . $datemonth . '-08",concat(idets.sale_prc,"_",i.id),null))) as agu'),
                DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "' . $datemonth . '-09",concat(idets.sale_prc,"_",i.id),null))) as sep'),
                DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "' . $datemonth . '-10",concat(idets.sale_prc,"_",i.id),null))) as okt'),
                DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "' . $datemonth . '-11",concat(idets.sale_prc,"_",i.id),null))) as nov'),
                DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "' . $datemonth . '-12",concat(idets.sale_prc,"_",i.id),null))) as des'),
                DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "' . $datemonth2 . '-01",concat(idets.sale_prc,"_",i.id),null))) as jan'),
                DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "' . $datemonth2 . '-02",concat(idets.sale_prc,"_",i.id),null))) as feb'),
                DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "' . $datemonth2 . '-03",concat(idets.sale_prc,"_",i.id),null))) as mar'),
                DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "' . $datemonth2 . '-04",concat(idets.sale_prc,"_",i.id),null))) as apr'),
                DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "' . $datemonth2 . '-05",concat(idets.sale_prc,"_",i.id),null))) as mei'),
                DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "' . $datemonth2 . '-06",concat(idets.sale_prc,"_",i.id),null))) as jni')
            )
        ->leftJoin('invoices AS i', function ($join) use ($request) {
            $join->on('users.id', '=', 'i.user_id')->where('thn_ajaran', $request->year_id);
        })
        ->leftJoin('user_classes AS uc', function ($join) {
            $join->on('users.id', '=', 'uc.user_id');
        })
        ->leftJoin('classes AS c', function ($join) {
            $join->on('uc.class_id', '=', 'c.id');
        })
        ->leftJoin('user_companies AS usercompany', 'usercompany.user_id', '=', 'users.id')
        ->leftJoin('invoices_dets AS idets', function ($join) {
          $join->on('idets.invoice_id', '=', 'i.id')
            ->join('products AS p', 'p.id', '=', 'idets.prod_id')
            ->where('p.type', '1');
        })
//        ->leftJoin('invoices_dets AS idets', 'i.id', '=', 'idets.invoice_id')
//        ->leftJoin('products AS p', function ($join) {
//            $join->on('idets.prod_id', '=', 'p.id')->where('p.type', '1');
//        })
        ->where('users.active', 1)
        ->where('users.type', 'S')
        ->where('c.id', $request->class_id)
        ->orderBy(DB::RAW('users.first_name'))
        ->groupBy('users.id');

            if ($cari = \Request::get('cariNis')) {
                $invoiceUsers = $invoiceUsers->where(function ($query) use ($cari) {
                    $query->where('users.first_name', 'LIKE', "%$cari%")
            ->orWhere('users.last_name', 'LIKE', "%$cari%")
            ->orWhere('users.nis', 'LIKE', "%$cari%");
                });
            }
            if ($request->has('user_company_id')) {
                if ($request->user_company_id != 2) {
                    $invoiceUsers = $invoiceUsers->where('usercompany.company_id', $request->user_company_id);
                }
            }
            $invoiceUsers = $invoiceUsers->get();
        }
        return $invoiceUsers;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return string[]
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          // 'invoice_no' => ['required'],
          'user_invoice_id' => ['required', 'integer'],
          'user_id' => ['required', 'integer'],
          'total_amt' => ['required', 'between:0,99.99'],
          // 'thn_ajaran' => ['required', 'integer'],
          // 'from_dt' => ['required', 'date'],
          // 'to_dt' => ['required', 'date']
        ]);

        if (!$request->has('prodType')) {
          $request->prodType = 1;
        }

        if ($request->month && is_array($request->month)) {
            DB::beginTransaction();

            try {
                foreach ($request->month as $key => $value) {
                    $year = Year::find($request->year);
                    if ($year) {
                        if (intval($value) > 6) {
                            $year = explode(' / ', $year->year_name)[0];
                        } else {
                            $year = explode(' / ', $year->year_name)[1];
                        }
                    } else {
                      return ['result' => 'error when select year', 'code' => '02'];
                    }

                    $insert = [
                      'from_dt' => date('Y-m-d', strtotime('01-' . $value . '-' . $year)),
                      'to_dt' => date('Y-m-t', strtotime('01-' . $value . '-' . $year)),
                      'invoice_no' => $this->selMaxInv().'_'.$request->prodType,
                      'user_id' => $request->user_invoice_id,
                      'thn_ajaran' => $request->year,
                      'total_amt' => $request->total_amt,
                      'company_id' => $request->company_id,
                      'insert_by' => $request->user_id,
                      'created_at' => date('Y-m-d H:i:s'),
                      'updated_at' => date('Y-m-d H:i:s'),
                      'ket' => $request->ket,
                    ];

                    $queryPrep = Invoices::where(['invoices.from_dt' => date('Y-m-d', strtotime('01-' . $value . '-' . $year)),
                        'invoices.to_dt' => date('Y-m-t', strtotime('01-' . $value . '-' . $year)),
                        'invoices.user_id' => $request->user_invoice_id
                      ])
                    ->where(DB::RAW('SUBSTRING(invoices.invoice_no, -2)'), '_'.$request->prodType);

                    $invCheck = $queryPrep->count();

                    Log::info('[InvoiceController@store] $invCheck is : ', [$invCheck]);
                    Log::info('[InvoiceController@store] $request->prodType is : ', [$request->prodType]);

                    if ($invCheck <= 0 || $request->prodType != 1) {
                        Log::info('[InvoiceController@store] begin insert to invoice.');

                        DB::table('invoices')->insert($insert);

                        Log::info('[InvoiceController@store] success insert to invoice.');
                        Log::info('[InvoiceController@store] select last id of invoice.');

                        $invSel = $queryPrep->latest('id')->first();
                        Log::info('[InvoiceController@store] last id of invoice is : ', [$invSel->id]);

                        if ($invSel) {
                            if ($request->prod_id && is_array($request->prod_id)) {
                                $invDetRows = [];
                                foreach ($request->prod_id as $key2 => $value2) {
                                    $invDetRows[] = [
                                      'invoice_id' => $invSel->id,
                                      'prod_id' => $value2,
                                      'prod_nm' => $request->prod_nm[$key2],
                                      'sale_prc' => $request->sale_prc[$key2],
                                      'insert_by' => $request->user_id,
                                      'created_at' => date('Y-m-d H:i:s'),
                                      'updated_at' => date('Y-m-d H:i:s')
                                    ];
                                }

                                Log::info('[InvoiceController@store] begin insert to invoice det');
                                Log::info('[InvoiceController@store] params is : ', $invDetRows);
                                DB::table('invoices_dets')->insert($invDetRows);

                                if ($request->prodType == 4) {
                                  Log::info('[InvoiceController@store] begin select product data');
                                  Log::info('[InvoiceController@store] prod id is : ', [$request->prod_id[0]]);
                                  $prod = Product::find($request->prod_id[0]);

                                  Log::info('[InvoiceController@store] product data is : ', $prod->toArray());
                                  Log::info('[InvoiceController@store] begin insert to Installment');

                                  $installment = new Installment();
                                  $installment->prod_id = $request->prod_id[0];
                                  $installment->user_id = $request->user_invoice_id;
                                  $installment->invoice_id = $invSel->id;
                                  $installment->installment_amount = $request->total_amt;
                                  $installment->loan_amount = $prod->sale_price;
                                  $installment->active = 1;
                                  $installment->insert_by = $request->user_id;

                                  Log::info('[InvoiceController@store] begin select last Installment');

                                  $selInstallment = Installment::where(
                                    [
                                      'user_id' => $request->user_invoice_id,
                                      'prod_id' => $request->prod_id[0],
                                      'active' => 1,
                                    ]
                                  )
                                  ->latest('id')->first();

                                  if ($selInstallment) {
                                    Log::info('[InvoiceController@store] Installment is : ', $selInstallment->toArray());
                                    $getSekInstallment = Installment::find($selInstallment->id);
                                    $installment->rest_of_loan = $getSekInstallment->rest_of_loan - $installment->installment_amount;
                                  } else {
                                    $installment->rest_of_loan = $installment->loan_amount - $installment->installment_amount;
                                  }

                                  $installment->save();
                                  Log::info('[InvoiceController@store] success insert to Installment');
                                }
                            }
                        }
                    }
                }

                DB::commit();
                return ['result' => 'Success', 'code' => '00', 'coba' => '_'.$request->prodType];
                // all good
            } catch (\Exception $e) {
                DB::rollback();
                return ['result' => $e, 'code' => '01'];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inv = Invoices::find($id);

        return $inv;
    }

    public function showInvoice($id)
    {
        $invoice = Invoices::find($id);
        if ($invoice) {
            $invoicesDet = InvoicesDet::where('invoice_id', $id)->get();
            if ($invoicesDet && count($invoicesDet) > 0) {
                // return ['invoice' => $invoice, 'invoicesDet' => $invoicesDet];
                return ['invoice' => $invoice, 'invoicesDet' => $invoicesDet, 'invoiceYear' => $invoice->years];
            }
            return ['invoice' => $invoice, 'invoicesDet' => []];
        }
        return ['invoice' => [], 'invoicesDet' => []];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
      'invoice_no' => ['required'],
      'user_id' => ['required', 'integer'],
      'total_amt' => ['required', 'between:0,99.99'],
      'thn_ajaran' => ['required', 'integer'],
      'from_dt' => ['required', 'date'],
      'to_dt' => ['required', 'date']
    ]);

        $input = $request->all();
        $input['update_by'] = Auth::user()->id;

        $invoice = Invoices::find($id);

        if ($invoice) {
            $invoice = $invoice->update($input);

            if ($invoice) {
                return ['result' => 'Success', 'code' => '00'];
            }
        }

        return ['result' => 'Error', 'code' => '01'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoice = Invoices::find($id)->delete();

        if ($invoice) {
            return ['result' => 'Success', 'code' => '00'];
        }

        return ['result' => 'Error', 'code' => '01'];
    }

    private function selMaxInv()
    {
        $inv = 0;
        $maxInv = Invoices::where(DB::RAW('SUBSTR(invoice_no, 5,6)'), date('ymd'))
        ->max('invoice_no');
          // ->toSql();
          // dd($maxInv);
          if (!$maxInv) {
              $inv = 'INV_' . date('ymd') . str_pad("1", 4, "0", STR_PAD_LEFT);
          } else {
              $inv = substr($maxInv, 4);
              $invArr = explode('_', $inv);
              $inv = intval($invArr[0]) + 1;
              $inv = 'INV_' . $inv;
          }
          return $inv;
    }

    public function getInvByCompId(Request $request)
    {
        Log::info('Start getInvByCompId');

        if (!$request->has('user_company_id')) {
            $message = ['msg' => 'Bad request, user company id must filled', 'code' => '0400'];
            Log::info('error : '.json_encode($message));
            return response()->json($message, 400);
        }

        if (!$request->has('year_id')) {
            $message = ['msg' => 'Bad request, year id must filled', 'code' => '0400'];
            Log::info('error : '.json_encode($message));
            return response()->json($message, 400);
        }

        Log::info('company id is : '.$request->user_company_id);
        Log::info('year id is : '.$request->year_id);

        $inv = Invoices::query();

        if ($request->has('user_company_id')) {
            $inv = $inv->where('company_id', $request->user_company_id);
        }

        if ($request->has('year_id')) {
            $inv = $inv->where('thn_ajaran', $request->year_id);
        }

        Log::info('query is : '.$inv->toSql());
        $inv = $inv->orderBy('id')->get();

        return response()->json($inv, 200);
    }


    public function getInvList(Request $request)
    {
        Log::info('Start getInvList');

        if (!$request->has('user_company_id')) {
            $message = ['msg' => 'Bad request, user_company_id must filled', 'code' => '0400'];
            Log::info('error : '.json_encode($message));
            return response()->json($message, 400);
        }

//        if (!$request->has('from_date')) {
//            $message = ['msg' => 'Bad request, from_date id must filled', 'code' => '0400'];
//            Log::info('error : '.json_encode($message));
//            return response()->json($message, 400);
//        }
//
//        if (!$request->has('to_date')) {
//            $message = ['msg' => 'Bad request, to_date must filled', 'code' => '0400'];
//            Log::info('error : '.json_encode($message));
//            return response()->json($message, 400);
//        }

        Log::info('company_id is : '.$request->user_company_id);
        Log::info('from_date is : '.$request->from_date);
        Log::info('to_date is : '.$request->to_date);

        $inv = Invoices::query();

        if ($request->has('user_company_id')) {
            $inv = $inv->where('company_id', $request->user_company_id);
        }

        if ($request->has('from_date')) {
            $inv = $inv->where('created_at', '>=', date('Y-m-d', strtotime($request->from_date)));
        }

        if ($request->has('to_date')) {
            $inv = $inv->where('created_at', '<=', date('Y-m-t', strtotime($request->to_date)));
        }

        if ($request->has('thn_ajaran')) {
          $inv = $inv->where('thn_ajaran', $request->thn_ajaran);
        }

        Log::info('query is : '.$inv->toSql());
        $inv = $inv->orderBy('id')->get();

        foreach ($inv as $val) {
            $val->dets = $val->dets;
            $val->user = $val->user;
            if ($val->user) {
                $val->user->class = $val->user->class;
            }
        }

        return response()->json($inv, 200);
    }

    public function exportExcel($companyId, $fromDate, $toDate)
    {
        Log::info('Start exportExcel');
        Log::info('company_id is : '.$companyId);
        Log::info('from_date is : '.$fromDate);
        Log::info('to_date is : '.$toDate);

        $inv = Invoices::query();

        $inv = $inv->where('company_id', $companyId)
        ->where('created_at', '>=', date('Y-m-d', strtotime($fromDate)))
        ->where('created_at', '<=', date('Y-m-t', strtotime($toDate)));

        Log::info('query is : '.$inv->toSql());
        $inv = $inv->orderBy('id')->get();

        $header[] = array("NO",	"Nama", "Kelas", "Nama Produk", "Nominal", "Tanggal Bayar", "Invoice No", "Penerima");

        $i=1;
        foreach ($inv as $key => $val) {
            $val->dets = $val->dets;
            $val->user = $val->user;
            if ($val->user) {
                $val->user->class = $val->user->class;
                $createdBy = User::find($val->insert_by);
                if ($createdBy) {
                  $penerima = $createdBy->first_name;
                  if ($createdBy->mid_name != null && $createdBy->mid_name != '') {
                    $penerima .= " ".$createdBy->mid_name;
                  }
                  if ($createdBy->last_name != null && $createdBy->last_name != '') {
                    $penerima .= " ".$createdBy->last_name;
                  }
                }
            }
            $class_name = '';
            if ($val->user->class != null) {
                $class_name = $val->user->class->class_name .' - '. $val->user->class->class_sub_name;
                if (isset($val->user->class->major)) {
                  $class_name = $val->user->class->class_name.$val->user->class->major .' - '. $val->user->class->class_sub_name;
                }
            }

            $prodName = '';
            if (count($val->dets) > 0) {
              $prodName = $val->dets[0]->prod_nm;
//              print_r($val->dets[0]->prod_nm);die;
            }

            if (isset($prodName) && $prodName != '') {
              $header[] = array(
                "No" => $i++,
                "Nama" => $val->user->first_name." ".$val->user->mid_name." ".$val->user->last_name,
                "Kelas" => $class_name,
                "Nama Produk" => $prodName,
                "Nominal" => $val->total_amt,
                "Tanggal Bayar" => $val->created_at,
                "Invoice No" => $val->invoice_no,
                "Penerima No" => $penerima??$val->insert_by
              );
            }
        }



        $export = new InvoicesExport($header);
        $nama_file = 'laporan_pembayaran_'.date('Y-m-d_H-i-s').'.xlsx';
        return Excel::download($export, $nama_file);
    }
}
