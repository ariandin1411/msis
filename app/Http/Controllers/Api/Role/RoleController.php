<?php

namespace App\Http\Controllers\Api\Role;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;

class RoleController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api', ['except' => ['']]);
      }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::where('active', 1)->get();

        if( $request->has('user_company_id') ){
            if( $request->user_company_id != 2 ) {
                $roles = $roles->where('company_id', $request->user_company_id);
            }
        }

        // $roles = $roles->get();

        foreach ($roles as $key => $value) {
            $value->company = $value->company;
        }

        return response()->json($roles, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'role_name' => ['required', 'string', 'max:191'],
        ]);

        $input = $request->all();
        $role = Role::create($input);
        if($role){
            return response()->json($role, 200);
        }

        $message = ['msg' => 'Error when input data', 'code' => '0500'];

        return response()->json($message, 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);

        if($role){
            return response()->json($role, 200);
        }

        $data = ['data not found'];
        return response()->json($data, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'role_name' => ['required', 'string', 'max:191'],
        ]);

        $role = Role::find($id);
        $input = $request->all();
        if($role){
            $roleUpdate = $role->update($input);
            if($roleUpdate){
                $role = Role::find($id);
                return response()->json($role, 200);
            }
        }

        $message = ['msg' => 'Error when update data', 'code' => '0500'];
        return response()->json($message, 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        if($role){
            $role->update(['active' => 0]);
            if(role){
                $message = ['msg' => 'Delete data success', 'code' => '0000'];
                return response()->json($message, 200);
            }
        }

        $message = ['msg' => 'Error when delete data', 'code' => '0500'];
        return response()->json($message, 500);
    }
}
