<?php

namespace App\Http\Controllers;

use App\Imports\StudentsImport;
use App\Models\Classes;
use App\Models\Company;
use Illuminate\Http\Request;
use Laravel\Passport\Client;
use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{

  public function __construct(){
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    return view('layouts.main');
  }

  public function upload(Request $request)
  {
    $companies = Company::where('active', 1)->get();
    return view('layouts.upload', ['companies' => $companies]);
  }

  public function getClasses(Request $request)
  {
    $classes = Classes::where('active', 1);

    if( $request->has('user_company_id') ){
      if( $request->user_company_id != 2 ) {
        $classes = $classes->where('company_id', $request->user_company_id);
      }
    }

    $classes = $classes->get();

    foreach ($classes as $key => $value) {
      $value->company = $value->company;
    }

    return response()->json($classes, 200);
  }

  public function storeUpload(Request $request)
  {
    Excel::import(new StudentsImport($request->classes, $request->company), $request->file('file'));
    // return response()->json([123], 200);
    return back();
  }
}
