<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Models\UserRole;
use App\Models\Role;
use App\Models\UserCompany;
use App\Models\Company;
use App\Models\StudentGuardian;
use App\Models\Classes;
use App\Models\UserClass;
use App\Models\UserGuardian;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password','first_name', 'mid_name', 'last_name',
        'nig', 'nis','type','gender','address','phone_number','phone_number2','year_of_entry',
        'active','insert_date','insert_by','update_date','update_by', 'profile_pic',
        'birth_date', 'born_at', 'religion', 'nisn'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','insert_by', 'update_by','created_at', 'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userRoles()
    {
        return $this->hasMany('App\Models\UserRole', 'user_id');
    }


    public function getRoleAttribute()
    {
      $getUserRole = UserRole::where('user_id', $this->id)->first();
      if( $getUserRole ){
        $role = Role::find($getUserRole->role_id);
        if($role){
          return $role;
        }
      }

      return "";
    }

    public function getCompanyAttribute()
    {
      $getUserCompany = UserCompany::where('user_id', $this->id)->first();
      if( $getUserCompany ){
        $company = Company::find($getUserCompany->company_id);
        if($company){
          return $company;
        }
      }

      return "";
    }

    public function getStudentGuardianAttribute()
    {
      $getStudentGuardian = StudentGuardian::where('user_id', $this->id)->first();
      if ( $getStudentGuardian ) {
          return $getStudentGuardian;
      }
      return null;
    }

    public function getClassAttribute()
    {
      $getUserClass = UserClass::where('user_id', $this->id)->first();
      if( $getUserClass ){
        $class = Classes::find($getUserClass->class_id);
        if($class){
          return $class;
        }
      }

      return "";
    }

    public function getUserGuardianAttribute()
    {
      $getUserGuardian = UserGuardian::where('guardian_user_id', $this->id)->get();
      if( $getUserGuardian ){
          foreach ($getUserGuardian as $val) {
            $getStudentFullName = User::find($val->student_user_id);
            if ($getStudentFullName) {
              $val->student_full_name = $getStudentFullName->first_name;
              $val->student_full_name .= ' '.$getStudentFullName->mid_name;
              $val->student_full_name .= ' '.$getStudentFullName->last_name;
            } else {
              $val->student_full_name = '';
            }
          }
          return $getUserGuardian;
      }
      return null;
    }
}
