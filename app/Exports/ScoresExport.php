<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;

class ScoresExport implements FromArray
{
    protected $scores;

    public function __construct(array $scores)
    {
        $this->scores = $scores;
    }

    public function array(): array
    {
        return $this->scores;
    }
}