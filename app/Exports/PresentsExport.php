<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;

class PresentsExport implements FromArray
{
    protected $presents;

    public function __construct(array $presents)
    {
        $this->presents = $presents;
    }

    public function array(): array
    {
        return $this->presents;
    }
}