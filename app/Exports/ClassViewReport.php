<?php

namespace App\Exports;

use App\Models\Classes;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ClassViewReport implements FromView
{
    public function view(): View
    {
        return view('kelas', [
            'datas' => Classes::all()
        ]);
    }
}