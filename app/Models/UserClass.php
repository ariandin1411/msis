<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserClass extends Model
{
    protected $fillable = [
        'user_id',
        'class_id',
        'active',
        'company_id',
        'insert_by',
        'update_by'
      ];
}
