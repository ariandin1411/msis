<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Company;

class Payment extends Model
{
    protected $fillable = ['payment_no', 'user_id', 'total_amt', 'thn_ajaran',
                          'from_dt', 'to_dt', 'insert_by', 'update_by', 'status',
                          'pay_method', 'txid', 'vacct_no', 'bank_cd', 'company_id'];

    public function dets()
    {
        return $this->hasMany('App\Models\PaymentDet');
    }

    public function years()
    {
        return $this->belongsTo('App\Models\Year', 'thn_ajaran');
    }

    public function getCompanyAttribute()
    {
        $getCompanyData = Company::find($this->company_id);
        if ($getCompanyData) {
            return $getCompanyData;
        }

        return "";
    }
}
