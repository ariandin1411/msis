<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoicesDet extends Model
{
    protected $fillable = ['invoice_id', 'prod_id', 'prod_nm', 'sale_prc', 'insert_by', 'update_by'];

    public function prod()
    {
      return $this->belongsTo('App\Models\Product', 'prod_id');
    }
}
