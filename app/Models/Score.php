<?php

namespace App\Models;

use App\Models\ScoreDetail;
use App\Models\ScoreType;
use App\Models\Classes;
use App\Models\Company;
use App\Models\Subject;
use App\User;


use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $fillable = [
        'user_id',
        'subject_id',
        'teacher_id',
        'class_id',
        'score_total',
        'active',
        'company_id',
        'year_id',
        'semester',
        'insert_by',
        'update_by'
      ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'insert_by', 'update_by','created_at', 'updated_at',
    ];
    
    public function getScoreDetailAttribute()
    {
        $getScoreDetail = ScoreDetail::where('score_id', $this->id)->get();
        if ($getScoreDetail) {
            foreach ($getScoreDetail as $key => $value) {
                $getScoreType =  ScoreType::where('id', $value->score_type_id)->first();
                // print_r($getScoreType);die;
                if ($getScoreType) {
                    $value->score_type =  $getScoreType;
                }
            }
           
            return $getScoreDetail;
        }
  
        return "";
    }

    public function getCompanyAttribute()
    {
        $getUserCompany = UserCompany::where('user_id', $this->id)->first();
        if ($getUserCompany) {
            $company = Company::find($getUserCompany->company_id);
            if ($company) {
                return $company;
            }
        }
  
        return "";
    }

    public function getClassesAttribute()
    {
        if (isset($this->class_id)) {
            $getClasses = Classes::find($this->class_id);
            if ($getClasses) {
                return $getClasses;
            }
        }

        return "";
    }

    public function getUserAttribute()
    {
        if (isset($this->user_id)) {
            $user = User::find($this->user_id);
            if ($user) {
                return $user;
            }
        }
        return "";
    }

    public function getTeacherAttribute()
    {
        if (isset($this->teacher_id)) {
            $teacher = User::find($this->teacher_id);
            if ($teacher) {
                return $teacher;
            }
        }
        return "";
    }

    public function getSubjectAttribute()
    {
        if (isset($this->subject_id)) {
            $subject = Subject::find($this->subject_id);
            if ($subject) {
                return $subject;
            }
        }
        return "";
    }
}
