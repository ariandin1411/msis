<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserGuardian extends Model
{
    protected $fillable = ['student_user_id', 'guardian_user_id', 'company_id','active','insert_by','update_by'];

    protected $hidden = [
        'insert_by', 'update_by','created_at', 'updated_at',
    ];

    public function getCompanyAttribute()
    {
        $company = Company::find($this->company_id);
        if ($company) {
            return $company;
        }
    }

    public function getUserStudentAttribute()
    {
        if (isset($this->student_user_id)) {
            $user_student = User::find($this->student_user_id);
            if ($user_student) {
                return $user_student;
            }
        }
        return "";
    }

    public function getUserGuardianAttribute()
    {
        if (isset($this->guardian_user_id)) {
            $user_guardian = User::find($this->guardian_user_id);
            if ($user_guardian) {
                return $user_guardian;
            }
        }
        return "";
    }
}
