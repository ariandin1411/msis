<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
                            'role_name',
                            'level',
                            'description',
                            'icon',
                            'active',
                            'insert_by',
                            'update_by'
                          ];


    public function getCompanyAttribute()
    {
        if (isset($this->company_id)) {
            $getCompanyData = Company::find($this->company_id);
            if ($getCompanyData) {
                return $getCompanyData;
            }
        }

        return "";
    }
}
