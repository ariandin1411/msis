<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $table = 'villages';

    protected $casts = [
        'meta' => 'array',
    ];

    public $timestamps = false;

    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_id');
    }

    public function getDistrictNameAttribute()
    {
        return $this->district->district_name;
    }

    public function getCityNameAttribute()
    {
        return $this->district->city->city_name;
    }

    public function getProvinceNameAttribute()
    {
        return $this->district->city->province->province_name;
    }
}
