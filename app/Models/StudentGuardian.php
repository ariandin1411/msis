<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentGuardian extends Model
{
    protected $fillable = ['user_id', 'name', 'phone_number','email', 'active','insert_by','update_by'];
}
