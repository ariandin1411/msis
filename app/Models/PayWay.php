<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PayWay extends Model
{
    protected $fillable = [
        'bank_code',
        'description',
        'created_at',
        'updated_at'
      ];
}
