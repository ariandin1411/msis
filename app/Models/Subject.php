<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = [
                            'subject_name',
                            'active',
                            'company_id',
                            'insert_by',
                            'update_by'
                          ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'insert_by',
      'update_by',
      'created_at',
      // 'updated_at',
    ];

    public function getCompanyAttribute()
    {
        if (isset($this->company_id)) {
            $getCompanyData = Company::find($this->company_id);
            if ($getCompanyData) {
                return $getCompanyData;
            }
        }

        return "";
    }
}
