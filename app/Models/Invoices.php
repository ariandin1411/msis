<?php

namespace App\Models;

use App\Models\Company;
use App\Models\InvoicesDet;
use Illuminate\Database\Eloquent\Model;

class Invoices extends Model
{
    protected $fillable = ['invoice_no', 'user_id', 'total_amt', 'thn_ajaran',
                          'from_dt', 'to_dt', 'insert_by', 'update_by', 'company_id', 'ket'];

    public function dets()
    {
        return $this->hasMany('App\Models\InvoicesDet', 'invoice_id', 'id');
    }

    public function user()
    {
      return $this->belongsTo('App\User', 'user_id');
    }

    public function years()
    {
        return $this->belongsTo('App\Models\Year', 'thn_ajaran');
    }

    public function getCompanyAttribute()
    {
        $getCompanyData = Company::find($this->company_id);
        if ($getCompanyData) {
            return $getCompanyData;
        }

        return "";
    }

    public function getDetailsAttribute()
    {
      $getDetails = InvoicesDet::where('invoice_id', $this->id)->get();
      if (count($getDetails) > 0) {
        return $getDetails;
      }

      return [];
    }
}
