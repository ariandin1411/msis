<?php

namespace App\Models;

use App\Models\Product;
use App\Models\User;
use App\Models\Invoice;
use Illuminate\Database\Eloquent\Model;

class installment extends Model
{
    protected $guarded = ['id'];

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->hasMany(Product::class);
    }
}
