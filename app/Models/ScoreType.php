<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScoreType extends Model
{
    protected $fillable = [
        'name',
        'weight',
        'active',
        'company_id',
        'insert_by',
        'update_by'
      ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'insert_by', 'update_by','created_at', 'updated_at',
  ];
}
