<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Company;

class Classes extends Model
{
    protected $fillable = ['class_name','class_sub_name','major','active','company_id','insert_by','update_by'];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
      'insert_by',
      'update_by',
      'created_at',
      // 'updated_at',
  ];

    public function getCompanyAttribute()
    {
        $getCompanyData = Company::find($this->company_id);
        if ($getCompanyData) {
            return $getCompanyData;
        }

        return "";
    }
}
