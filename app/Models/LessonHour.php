<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Company;

class LessonHour extends Model
{
    protected $fillable = [
                            'time_name',
                            'from_time',
                            'to_time',
                            'time_interval',
                            'active',
                            'company_id',
                            'insert_by',
                            'update_by'
                          ];

    public function getCompanyAttribute()
    {
        if (isset($this->company_id)) {
            $getCompanyData = Company::find($this->company_id);
            if( $getCompanyData ){
                return $getCompanyData;
            }
        }

        return "";
    }
}
