<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Company;
use App\Models\Classes;

class Product extends Model
{
    protected $fillable = [
                            'product_no',
                            'product_name',
                            'company_id',
                            'class_id',
                            'buy_price',
                            'sale_price',
                            'tax',
                            'insert_by',
                            'update_by',
                            'active',
                            'type'
                          ];

    public function getCompanyAttribute()
    {
        $getCompanyData = Company::find($this->company_id);
        if ($getCompanyData) {
            return $getCompanyData;
        }

        return "";
    }

    public function getClassAttribute()
    {
        $class = Classes::find($this->class_id);
        if ($class) {
            return $class;
        }

        return "";
    }
}
