<?php

namespace App\Models;

use App\Models\Classes;
use Illuminate\Database\Eloquent\Model;
use App\Models\Company;
use App\Models\LessonHour;
use App\User;

class Schedule extends Model
{
    protected $fillable = [
                            'subject_id',
                            'user_id', // teacher id
                            'lesson_hour_id',
                            'day_name',
                            'schedule_type', //regular, extrakulikuler,  private class
                            'year_id',
                            'semester',
                            'class_id',
                            'company_id',
                            'active',
                            'insert_by',
                            'update_by'
                          ];

    public function getCompanyAttribute()
    {
        if (isset($this->company_id)) {
            $getCompanyData = Company::find($this->company_id);
            if( $getCompanyData ){
                return $getCompanyData;
            }
        }

        return "";
    }

    public function getUserAttribute()
    {
        if (isset($this->user_id)) {
            $getUserData = User::find($this->user_id);
            if( $getUserData ){
                return $getUserData;
            }
        }

        return "";
    }

    public function getLessonHourAttribute()
    {
        if (isset($this->lesson_hour_id)) {
            $getLessonHour = LessonHour::find($this->lesson_hour_id);
            if( $getLessonHour ){
                return $getLessonHour;
            }
        }

        return "";
    }


    public function getClassesAttribute()
    {
        if (isset($this->class_id)) {
            $getClasses = Classes::find($this->class_id);
            if( $getClasses ){
                return $getClasses;
            }
        }

        return "";
    }

    public function getSubjectAttribute()
    {
        if (isset($this->subject_id)) {
            $getSubject = Subject::find($this->subject_id);
            if( $getSubject ){
                return $getSubject;
            }
        }

        return "";
    }
}
