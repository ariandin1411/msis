<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScoreDetail extends Model
{
    protected $fillable = [
        'score_id',
        'score_type_id',
        'score',
        'active',
        'insert_by',
        'update_by'
      ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'insert_by', 'update_by','created_at', 'updated_at',
  ];
}
