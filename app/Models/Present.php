<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Company;
use App\Models\Classes;
use App\Models\UserClass;

class Present extends Model
{
    protected $fillable = ['user_id', 'class_id', 'desc', 'year_id', 'semester', 'company_id', 'active', 'insert_by', 'update_by', 'date_in'];

    public function getCompanyAttribute()
    {
        $getCompanyData = Company::find($this->company_id);
        if ($getCompanyData) {
            return $getCompanyData;
        }

        return "";
    }

    public function getClassAttribute()
    {
        $getUserClass = UserClass::where('user_id', $this->user_id)->first();
        if ($getUserClass) {
            $class = Classes::find($getUserClass->class_id);
            if ($class) {
                return $class;
            }
        }

        return "";
    }

    public function user()
    {
      return $this->belongsTo('App\User', 'user_id');
    }
    public function classes()
    {
      return $this->belongsTo('App\Models\Classes', 'class_id');
    }
  
}
