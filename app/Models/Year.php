<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $fillable = [
        'year_name',
        'current_year',
        'active',
        'insert_by',
        'update_by'
      ];
}
