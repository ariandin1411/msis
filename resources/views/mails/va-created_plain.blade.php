Hello {{ $va->receiver }},
Tagihan anda untuk bayar bulanan sekolah berhasil terbuat, 
silahkan bayar tagihan anda dengan data di bawah.

Bank : {{ $va->bankCd }}
Atas Nama : {{ $va->billingNm }}
tXid : {{ $va->tXid }}
Total Bayar : Rp. {{ number_format($va->amt) }}
No Va : {{ $va->vacctNo }}
Valid Samapai : {{ substr($va->vacctValidDt, 0, 2) }}-{{ substr($va->vacctValidDt, 4, 2) }}-{{ substr($va->vacctValidDt, 6, 2) }}, {{ substr($va->vacctValidTm, 0, 2) }}:{{ substr($va->vacctValidTm, 2, 2) }}

Cara Pembayaran
{!! $va->payWay !!}

Thank You,
{{ $va->sender }}
