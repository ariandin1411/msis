<!DOCTYPE html>

</html>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style type="text/css">
        body {
            padding: 10px;
        }

        tr td {
            padding: 10px;
        }

        ul {
            list-style-type: none;
        }

        ul li {
            padding: 10px;
        }

        ul li::before {
            content: "\00BB";
            color: rgb(44, 130, 201);
            font-weight: bold;
            display: inline-block;
            width: 1em;
            margin-left: -1em;
        }

    </style>
</head>

<body>


    <div class="container">
        <h5>
            <img src="http://msis.my.id/img/msis_logo.8e1ce6b1.png" width="100px" class="fr-fic fr-dii">
        </h5>
        <hr>
        <p>
            <strong>
                <span style="font-size: 24px;">Hello,&nbsp;</span>
                <span style="color: rgb(44, 130, 201); font-size: 24px;"> {{ $va->billingNm }}</span>
            </strong>
        </p>
        <p>Tagihan anda untuk
            <strong>
                <span style="color: rgb(44, 130, 201);">Pembayaran Sekolah</span>&nbsp;
            </strong>telah berhasil dibuat,
        </p>
        <p>Silahkan segera bayar tagihan anda dengan data sebagai berikut :</p>
        <table class="table">
            <tr>
                <td>Bank</td>
                <td>: {{ $va->bankCd }}</td>
            </tr>

            <tr>
                <td>Atas Nama</td>
                <td>: {{ $va->billingNm }}</td>
            </tr>

            <tr>
                <td>tXid</td>
                <td>: {{ $va->tXid }}</td>
            </tr>

            <tr>
                <td>Total Bayar</td>
                <td>: Rp. {{ number_format($va->amt) }}</td>
            </tr>

            <tr>
                <td>No Va</td>
                <td>: {{ $va->vacctNo }}</td>
            </tr>

            <tr>
                <td>Valid Samapai</td>
                <td>:
                    {{ substr($va->vacctValidDt, 0, 2) }}-{{ substr($va->vacctValidDt, 4, 2) }}-{{ substr($va->vacctValidDt, 6, 2) }},
                    {{ substr($va->vacctValidTm, 0, 2) }}:{{ substr($va->vacctValidTm, 2, 2) }}</td>
            </tr>
        </table>
        <br>
        <span>
            <h5><b><span style="color: rgb(44, 130, 201);">Cara Pembayaran</span></b></h5>
        </span>

        <h2>Cara Pembayaran</h2>,
        {!! $va->payWay !!}
        <br />
        Thank You,
        <br />
        <i>{{ $va->sender }}</i>
    </div>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>
</body>

</html>
