@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level === 'error')
# @lang('Whoops!')
@else
# @lang('Hello!')
@endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

{{-- Action Button --}}
@php
    $actionUrlEncoded = json_encode($actionUrl);
    $actionUrlEncoded = str_replace("127.0.0.1:8000","msis.my.id",$actionUrlEncoded);
    $actionUrlEncoded = str_replace("password\/","",$actionUrlEncoded);
    $actionUrlDecoded = json_decode($actionUrlEncoded);
    echo "<a href='".$actionUrlDecoded."'>Reset Password</a>";
@endphp


{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('Regards'),<br>
{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang(
    "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
    'into your web browser:',
    [
        'actionText' => $actionText,
    ]
)
@php
echo "<a href='".$actionUrlDecoded."'>".$actionUrlDecoded."</a>";
@endphp
@endslot
@endisset
@endcomponent

